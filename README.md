# Patronload

Patronload is a tool to create marc files (containing student records) suitable for innopac ingestion (for the University library’s book check-out system).  The student records are retrieved from the ACT Sybase database and, though this patronload application, data is formed into marc files that are generated automatically each morning. The user would then download the marc file from this application and upload it into innopac to update the student records.

## Local Development
The most effective way to do local development is by using the [k3d][k3d] tool
maintained by Rancher to provision and run an instance of the [Patron Load Helm
chart][patronload-helm] within a local [k3s][k3s] cluster on your workstation.

There is a `Makefile` in the repository which you can use to setup the cluster,
build and push images to the local image registry, and deploy.

You can run `make` to get a list of options, or review the `Makefile` directly.

Note: You will need to create a file in the `scripts` directory called `k3d-private.yaml` and populate it with the following yaml values (update with real secrets):

The values are set in LastPass in the `Application-Patronload-Secrets` Secure Note.

```yaml
oracleDB2:
  username: username
  password: password
  host: mydb2:5050
  database: database

ldap:
  username: ldap-user
  password: ldap-pass

minio:
  awsAccessKey: access-key
  awsSecretKey: secret-key
  endpointUrl: https://minio-endpoint
  bucket: bucket

almaMinio:
  awsAccessKey: access-key
  awsSecretKey: secret-key
  endpointUrl: https://your-minio-or-s3:9000
  bucket: thebucket

sftp:
  server: server
  path: /path
  username: username
  password: password
```

On Mac and Windows boxes, you will need to add the following to either /etc/hosts ( on a Mac ) or c:\windows\system32\drivers\etc\hosts ( on Windows )

127.0.0.1 k3d-registry.localhost\
127.0.0.1 patronload.k3d.localhost

Run the application:

```sh
make deploy
```

Once deployed, you can point your browser to:

```
http://patronload.k3d.localhost/patronload/
```

### Local Development Workflow
Initially, you will want to use `make deploy` to get the Helm deployment in
place and ensure everything is setup properly in the k3s cluster via `k3d`.

However, when you are working on the application code itself, a much faster
solution to see your code changes reflected in the k3s cluster will be to use:

```
make redeploy
```

This task leverages the Tomcat manager application that is enabled in the
`development` target in the `Dockerfile`. It does the following:

* Builds a new `war` file for the application using the `builder` target
* Copies that `war` file to the `./tmp` folder on your host machine
* Uses `curl` to upload the `war` file to the running Tomcat manager application inside the cluster, and redeploys automatically

This is significantly faster than building a new container image and deploying
it into the cluster.

[k3d]:https://github.com/rancher/k3d/
[k3s]:https://github.com/k3s-io/k3s
