#!/usr/bin/env sh
pod="$(kubectl get pods --namespace=billing-circ-development "--selector=app.kubernetes.io/name=billing-circ" --no-headers | cut -d " " -f 1)"

# TODO: if we ever have AP Batch Junit tests, follow a patter similar to this for rspec
# kubectl exec -it --namespace=billing-circ-development "$pod" -- /bin/sh -c "bundle exec rspec $*"

