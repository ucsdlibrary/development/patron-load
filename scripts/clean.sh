#!/usr/bin/env sh

echo "Deleting k3s cluster for UCSD Library Development..."
k3d cluster delete ucsdlibrary-dev
