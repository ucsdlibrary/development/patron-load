#!/usr/bin/env sh

namespace="patron-load-development"
context="k3d-ucsdlibrary-dev"
release=${RELEASE_NAME:=development}
values_file=${VALUES_FILE:=scripts/k3d.yaml}
private_values_file="scripts/k3d-private.yaml"
registry_port=${REGISTRY_PORT:=41906}
image_repository="k3d-registry.localhost:$registry_port/patron-load_web"
git_sha="$(git rev-parse HEAD)"

if test ! -f "$(pwd)/$private_values_file"; then
  echo "No scripts/k3d-private.yaml file found..."
  echo "Please add one, and add secrets such as db2, ftp, and ldap credentials..."
  echo "The secrets are stored in Lastpass: Shared-Library-Devops/Application-Patronload-Secrets..."
  exit 1
fi

if docker image inspect "$image_repository:$git_sha" > /dev/null 2>&1; then
  echo "patron-load container image already exists in Registry, skipping build..."
else
  echo "Building and pushing patron-load container image to Registry..."
  # shellcheck disable=SC1091
  . ./scripts/build.sh
fi

if kubectl --context $context get namespaces | grep -q "patron-load-development"; then
  echo "Namespace patron-load-development already exists, skipping creation..."
else
  echo "Creating namespace for deployment..."
  kubectl --context $context create namespace "$namespace"
fi

echo "Ensuring patron-load Helm chart dependencies are installed..."
helm dep up ./chart

echo "Deployment patron-load using Helm chart into k3d cluster..."
helm upgrade \
  --timeout 30m0s \
  --atomic \
  --install \
  --kube-context="$context" \
  --namespace="$namespace" \
  --set image.repository="$image_repository" \
  --set image.tag="$git_sha" \
  --values="$values_file" \
  --values="$private_values_file" \
  "$release" \
  ./chart
