#!/usr/bin/env sh
pod="$(kubectl get pods --namespace=patron-load-development "--selector=app.kubernetes.io/name=patron-load" --no-headers | cut -d " " -f 1)"

kubectl exec -it --namespace=patron-load-development "$pod" -- /bin/sh

