#!/usr/bin/env sh
registry_port=${REGISTRY_PORT:=41906}
git_sha="$(git rev-parse HEAD)"
image_tag="k3d-registry.localhost:$registry_port/patron-load_web:${git_sha}"
target=${PATRONLOAD_DOCKERFILE_TARGET:=development}

echo "Building patron-load image..."
docker build --target "$target" -t "$image_tag" -f Dockerfile .

echo "Pushing patron-load image to local registry..."
docker push "$image_tag"
