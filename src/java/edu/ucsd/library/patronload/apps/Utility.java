package edu.ucsd.library.patronload.apps;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;

import edu.ucsd.library.shared.Http;

import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Utility {

    /**
     * Retrieve the db2 datasource from JNDI
     * @return
     * @throws NamingException
     * @throws SQLException
     */
    public static Connection getDb2Connection() throws NamingException, SQLException {
        InitialContext ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/actDB2");
        return ds.getConnection();
    }

    public static String getToken(String filePath, String fileName, String scriptName) {
        Process process = null;
        PrintWriter printWriter = null;
        String token = null;
        try {
            process = Runtime.getRuntime().exec(filePath + scriptName);
            InputStream inputStream = process.getInputStream();
            printWriter = new PrintWriter(new BufferedOutputStream(new FileOutputStream(
                filePath + fileName)));
            printWriter.print(convert(inputStream));
            printWriter.close();
            FileReader reader = new FileReader(filePath + fileName);
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject)jsonParser.parse(reader);
            token = jsonObject.get("access_token").toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                process.destroy();
                if (printWriter != null)
                  printWriter.close();
            } catch (Exception e) {}
        }
        return token;
    }

    public static String convert(InputStream inputStream) throws IOException{
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        return result.toString("UTF-8");
    }

    public static String systemID(String id, String type, String token) {
        String sysId = null, body = null;
        try {
            GetMethod rdfGet = null;

        	rdfGet = new GetMethod("https://api.ucsd.edu:8243/systemid-rest/0.1P/id/"+type+"/"+id.toUpperCase()+"/library");
            rdfGet.setRequestHeader("Accept", "application/json");
            rdfGet.setRequestHeader("Authorization", "Bearer "+token);
            body = Http.execute( rdfGet );
            if(body != null) {
            	Object obj = JSONValue.parse(body.toString());
            	JSONObject jsonObj = (JSONObject) obj;
            	sysId = null;
            	if( jsonObj != null && jsonObj.containsKey("systemId")) {
            	    sysId = String.valueOf(jsonObj.get("systemId"));
            	}
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sysId;
    }

    public static void postSystemID(String id, String type, String token) {
    	int responseCode = 0;
    	PostMethod postMethod = null;
        try {
             HttpClient httpClient = new HttpClient();
             postMethod = new PostMethod("https://api.ucsd.edu:8243/systemid-rest/0.1P/id/"+type+"/"+id.toUpperCase()+"/library");
         	 System.out.println("postURL:"+"https://api.ucsd.edu:8243/systemid-rest/0.1P/id/"+type+"/"+id.toUpperCase()+"/library");

             postMethod.setRequestHeader("Accept", "application/json");
             postMethod.setRequestHeader("Authorization", "Bearer "+token);

             responseCode = httpClient.executeMethod(postMethod);
             if (responseCode != 200) {
                 System.out.println("Failed posting responseCode:"+responseCode+" for ID:"+id);
                 postMethod.releaseConnection();
             }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        	if(postMethod != null)
                postMethod.releaseConnection();
        }
    }

    public static void delay(final long durationInMillis) {
	    delay(durationInMillis, TimeUnit.MILLISECONDS);
	}

	public static void delay(final long duration, final TimeUnit unit) {
	    long currentTime = System.currentTimeMillis();
	    long deadline = currentTime+unit.toMillis(duration);
	    ReentrantLock lock = new ReentrantLock();
	    Condition waitCondition = lock.newCondition();

	    while ((deadline-currentTime)>0) {
	        try {
	            lock.lockInterruptibly();
	            waitCondition.await(deadline-currentTime, TimeUnit.MILLISECONDS);
	        } catch (InterruptedException e) {
	            Thread.currentThread().interrupt();
	            return;
	        } finally {
	            lock.unlock();
	        }
	        currentTime = System.currentTimeMillis();
	    }
	}

	public static Map loadSystemId(String systemIdQuery) {
        Map systemIdMap = new HashMap();
        ResultSet systemIdRS = null;
        PreparedStatement pstmt = null;
        Connection db2Conn = null;
        try {
            db2Conn = Utility.getDb2Connection();
			pstmt = db2Conn.prepareStatement(systemIdQuery);

		    systemIdRS = pstmt.executeQuery();
		    while (systemIdRS.next()) {
		    	systemIdMap.put((String)systemIdRS.getString(1).trim(), (String)systemIdRS.getString(2));
		    }
		    loadSystemIdFile(systemIdMap);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        	try {
        	    if (db2Conn != null)
				    db2Conn.close();
        	} catch (SQLException e) {}
        }
        return systemIdMap;
	}
	
	public static void loadSystemIdFile(Map systemIdMap) {
		String marcFilesDir = null;
        BufferedReader in = null;
	    try {
	        InitialContext jndi = new InitialContext();
	        marcFilesDir = (String)jndi.lookup("java:comp/env/marcFilePath");
	    } catch (NamingException e) {
	        e.printStackTrace();
	    }
        try {
            in = new BufferedReader(new FileReader(marcFilesDir + "noSystemIdPatrons.txt"));
            String lineIn = null;
            String[] strArray = null;
            while(((lineIn = in.readLine()) != null) && !(lineIn.trim().equals(""))) {                    
    	        strArray = lineIn.trim().split("-");	            	    	
		    	if(systemIdMap.containsKey(strArray[0].trim())) {
		    		System.out.println(strArray[0].trim()+"-duplicate key:"+systemIdMap.get(strArray[0].trim()));
		    	} else if(strArray.length == 2 && !lineIn.contains("null") && !strArray[1].isEmpty()){
                	systemIdMap.put(strArray[0].trim(), strArray[1].trim());
		    	}
            }    
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (Exception e) {}
        }
	}
}
