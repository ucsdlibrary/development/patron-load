/*
 * Created on Sep 11, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package edu.ucsd.library.patronload.apps;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import edu.ucsd.library.util.FileUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringEscapeUtils;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

/**
 * @author jjesena
 * @author tchu
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class fullquery_employee {

	/**
	 * @param args the command line arguments
	 * args[0] = path to affiliations properties file
	 * args[1] = path to patron type codes properties file
	 * args[2] = path to original properties file
	 * args[3] = file to write output to
	 * args[4] = boolean value; true=get everything; false=get only active empl.
	 * args[5] = list of IDs
	 */
	public static void main(String args[]) {

		if ((args == null) || (args.length < 6)) {
			System.out.println(
				"\nSyntax: java fullquery [affiliations properties file] [patron type codes properties file] [original properties file] [fileToWrite] [true=get all; false=get only active empl]");
		} else {
			grabData(args[0], args[1], args[2], args[3], args[4].equals("true") ? true : false, args[5]);
		}
	}

	private static void writeFile(StringBuffer in, String file) {
		if (in.toString().length() > 0) {
			try {
				FileUtils.stringToFile(in.toString(), outputLocation + file);
			} catch (IOException ioe) {
			}
		}
	}

	/**
	 * Write data out to the file
	 * @param props1 Path to affilications properties file
	 * @param props2 Path to type codes properties file
	 * @param props3 Path to original properties file
	 * @param fileToWrite Path to the file to write results to
	 * @param total Boolean value: true=get everything; false=get active empl. only
	 */
	public static void grabData(
		String props1,
		String props2,
		String props3,
		String fileToWrite,
		boolean total,
		String idList) {

		outputLocation = fileToWrite.substring(0, fileToWrite.lastIndexOf(File.separator)) + File.separator;

		PrintWriter pw = null;

		//--- Create the file stream here to output to file
		try {
			OutputStream os = new FileOutputStream(fileToWrite);
			pw = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
			downloadCsv();
			transformCsv();
			getRawDataCsv(props1, props2, props3, pw, total, idList);

			//go ahead and write the debug files
			writeFile(filter3a, "remove-step3a.txt");
			writeFile(filter4a, "remove-step4a.txt");
			writeFile(filter4b, "remove-step4b.txt");
			writeFile(filter4c, "remove-step4c.txt");
			writeFile(filter4d, "remove-step4d.txt");
			writeFile(typeCodeNotFound, "type_code_not_found.txt");
			writeFile(titleCodeNotFound, "title_code_not_found.txt");
			writeFile(affiliationCodeNotFound, "affiliation_code_not_found.txt");
			writeFile(moreDuplicates, "more_dups.txt");
			writeFile(invalidRecords, "remove-invalid.txt");

		} catch (IOException ioe) {
			System.out.println(ioe);
		} finally {
			if (pw != null)
				pw.close();
		}
	}

	private static void transformCsv() {
		String inputFileName = outputLocation+"sourceData.csv";
		String outputFileName = outputLocation+"sourceDataTmp.csv";
		BufferedReader reader = null;
	    PrintWriter pw = null;
        try {
       	      Reader inputReader = new InputStreamReader(new FileInputStream(inputFileName),"UTF-8");
        	  reader = new BufferedReader(inputReader);        	
        	  OutputStream os = new FileOutputStream(outputFileName);
			  pw = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));       	
	          String lineIn = "";
	          String[] data = null;
	          Vector problemData = new Vector();
	          int count = 0, index = 0;
	          while (((lineIn = reader.readLine()) != null)
	              && !(lineIn.trim().equals(""))) {
	                  lineIn = lineIn.trim();
	                  if(lineIn.contains("\\\"")) {
	                	  index = lineIn.indexOf("\\\"");
	                	  data = lineIn.split(",");
	                	  problemData.add(data[2]+","+lineIn.substring(index, index + 20 ));
                          lineIn = lineIn.replaceAll("\\\\\"", "");
	                  }
	 	              pw.write(lineIn + "\n");
	              count++;
	          }  	
	          pw.close();
              if (problemData.size() > 0) {
            	  SendEmail.execute(problemData);
              }
	      } catch (Exception ioe) {
	          System.out.println(ioe);
	      } finally {
	          try {
	              pw.close();
	              reader.close();
	          } catch (Exception e) {
	          }
	      } 
	}

	private static void downloadCsv() {
		String localFileName = outputLocation+"sourceData.csv";
		ChannelSftp channelSftp = null;
		try {
			File tmp = new File(localFileName);
			if (tmp.exists()) {
				System.out.println("Delete existing sourceData file");
				tmp.delete();
			}
			if (tmp.exists()) {
				System.out.println("Failed to delete existing sourceData file");
			}
			channelSftp = setupJsch();
		    String remoteFile = PATHNAME + "/PatronLoad_EAH_Workforce_Downstream_Systems_Consolidated.csv";
		    channelSftp.connect();
		    channelSftp.get(remoteFile, localFileName);
		} catch (JSchException je) {
			System.out.println("$$$ JSchException:"+je);
		} catch (SftpException e) {
			System.out.println("$$$ SftpException:"+e);
			e.printStackTrace();
		} finally {
	        channelSftp.exit();
		}
	}

	public static ChannelSftp setupJsch() throws JSchException {
		String hostname = null, known_hosts = null, username = null, password = null;
		JSch jsch = null;
		Session jschSession = null;
		try {
		    InitialContext context = new InitialContext();
		    hostname =
		          (String)context.lookup("java:comp/env/patronloadFtpServer/hostname");
		    PATHNAME =
		         	 (String)context.lookup("java:comp/env/patronloadFtpServer/path");
		    username =
		         	 (String)context.lookup("java:comp/env/patronloadFtpServer/username");
		    password =
		         	 (String)context.lookup("java:comp/env/patronloadFtpServer/password");
		    known_hosts = (String)context.lookup("java:comp/env/knownHostsPath");
		} catch(NamingException nee) {
			System.out.println("$$$ NamingException:"+nee);
		}

		try {
	        jsch = new JSch();
	        jsch.setKnownHosts(known_hosts);
	        jschSession = jsch.getSession(username, hostname);
	        jschSession.setPassword(password);
	        jschSession.connect();
		} catch (JSchException je) {
			System.out.println("$$$ JSchException in setupJsch:"+je);
			je.printStackTrace();
		}

	    return (ChannelSftp) jschSession.openChannel("sftp");
	}
	/**
	 * Removes leading zeros from the String
	 * @param str The String to remove leading zeros from
	 * @return The String without any leading zeros
	 */
	private static String removeLeadingZeros(String str) {
		if (str == null) {
			return null;
		}
		char[] chars = str.toCharArray();
		int index = 0;
		for (; index < str.length(); index++) {
			if (chars[index] != '0') {
				break;
			}
		}
		return (index == 0) ? str : str.substring(index);
	}

	/**
	 * Parses a record String 'in' - it returns the String field at position 'num'
	 * Note: fields are separated by a tab character
	 * @param in The String to parse
	 * @param num The field position to return
	 * @return String - The field at position 'num'
	 */
	public static String parseRecord(String in, int num) {
		String[] array = in.split("\t");

		  //remove leading zeros except in mailcode case
		if(num != 17) {
			array[num] = removeLeadingZeros(array[num]);
			return array[num];
		} else
			return "none";
	}

  public static String parseRecord(String in, int num, boolean removeZero) {
		String[] array = in.split("\t");

    if(removeZero)
		  //remove leading zeros except in mailcode case
		  array[num] = removeLeadingZeros(array[num]);

		return array[num];
	}
	/**
	 * Checks to see if the provided titleCode is an Emeritus title code
	 * @param titleCode The title code to test
	 * @return boolean true if titleCode is an Emeritus title code; else false
	 */
	public static boolean isEmeritusTitleCode(String titleCode) {
		titleCode = titleCode.trim();
		if (EMERITUS_TITLE_CODES.indexOf("'" + titleCode + "'") > -1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks to see if the provided status code is a 'separated' status code
	 * @param status The status code to test
	 * @return boolean true if statusCode is a 'separated' statusCode; else false
	 */
	public static boolean isSeparatedStatus(String status) {
		status = status.trim();
		if (SEPARATED_CODES.indexOf("'" + status + "'") > -1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Tests if a patron record is separated
	 * @param line The patron record to test
	 * @return boolean true if patron is separated; else false
	 */
	public static boolean isPatronSeparated(String line) {
		String statusCode = parseRecord(line, 2).trim();
		return isSeparatedStatus(statusCode);
	}

	/**
	 * Tests if a patron is regarded as an Emeritus
	 * @param line The patron record to test
	 * @return boolean true if patron is granted emeritus status; else false
	 */
	public static boolean isPatronEmeritus(String line) {
		String titleCode = parseRecord(line, 4).trim();
		return isEmeritusTitleCode(titleCode);
	}

	/**
	 * Returns the staff title codes as a string surrounded by quotes; these
	 * codes are then separated by a comma.
	 * @param props The Properties file to get the codes from
	 * @return String - The staff title codes as a String
	 */
	private static String getStaffTitleCodes(Properties props) {

		StringBuffer result = new StringBuffer("(");
		for (Enumeration e = props.propertyNames(); e.hasMoreElements();) {

			String value = (String) e.nextElement();
			if (props.get(value).equals("17")) {
				result.append("'" + value + "',");
			}
		}

		result.append("'')");
		return result.toString();
	}

	/**
	 * Returns the faculty title codes as a string surrounded by quotes; these
	 * codes are then separated by a comma.
	 * @param props The Properties file to get the codes from
	 * @return String - The faculty title codes as a String
	 */
	private static String getFacultyTitleCodes(Properties props) {

		StringBuffer result = new StringBuffer("(");
		for (Enumeration e = props.propertyNames(); e.hasMoreElements();) {

			String value = (String) e.nextElement();
			if (props.get(value).equals("1")) {
				result.append("'" + value + "',");
			}
		}

		result.append("'')");
		return result.toString();
	}

	/**
	 * Returns the visiting patron title codes as a string surrounded by quotes; these
	 * codes are then separated by a comma.
	 * @param props The Properties file to get the codes from
	 * @return String - The visiting patron title codes as a String
	 */
	private static String getVisitingTitleCodes(Properties props) {

		StringBuffer result = new StringBuffer("(");
		for (Enumeration e = props.propertyNames(); e.hasMoreElements();) {

			String value = (String) e.nextElement();
			if (props.get(value).equals("41") || props.get(value).equals("42")) {
				result.append("'" + value + "',");
			}
		}

		result.append("'')");
		return result.toString();
	}
	/**
	 * Returns the student title codes as a string surrounded by quotes; these
	 * codes are then separated by a comma.
	 * @param props The Properties file to get the codes from
	 * @return String - The student title codes as a String
	 */
	private static String getStudentTitleCodes(Properties props) {

		StringBuffer result = new StringBuffer("(");
		for (Enumeration e = props.propertyNames(); e.hasMoreElements();) {

			String value = (String) e.nextElement();
			if (props.get(value).equals("12")) {
				result.append("'" + value + "',");
			}
		}

		result.append("'')");
		return result.toString();
	}

	/**
	 * Returns the post-doc title codes as a string surrounded by quotes; these
	 * codes are then separated by a comma.
	 * @param props The Properties file to get the codes from
	 * @return String - The post-doc title codes as a String
	 */
	private static String getPostDocTitleCodes(Properties props) {

		StringBuffer result = new StringBuffer("(");
		for (Enumeration e = props.propertyNames(); e.hasMoreElements();) {

			String value = (String) e.nextElement();
			if (props.get(value).equals("40")) {
				result.append("'" + value + "',");
			}
		}

		result.append("'')");
		return result.toString();
	}

	/**
	 * Performs step-3 filtering: "Software removes all listing for Code 3 employees
	 * with non-staff job titles"
	 * @param recordBuffer The Vector that potentially holds duplicates; what we are trying to clean
	 * @param typeCodes The Properties file to get the employee codes from
	 * @return Vector - The result of applying step-3 filtering
	 */
	private static Vector filter3a(Vector recordBuffer, Properties typeCodes) {
		int index = 0;

		while (index < recordBuffer.size()) {
			String tmpCurrent = (String) recordBuffer.elementAt(index);

			//check if this is a code 3 employee
			if (parseRecord(tmpCurrent, 8).equals("3")) {
				if (getStaffTitleCodes(typeCodes)
					.indexOf("'" + parseRecord(tmpCurrent, 4) + "'")
					== -1) {
					//if it's not part of the staff title codes, remove it

					filter3a.append(tmpCurrent);

					recordBuffer.removeElementAt(index);
				} else {
					index++;
				}
			} else {
				index++;
			}
		}

		return recordBuffer;
	}

	/**
	 * Performs step-4a filtering: "If any listing has department code=256, then retain this listing
	 * and delete all other listings for that employee"
	 * @param recordBuffer The Vector that potentially holds duplicates; what we are trying to clean
	 * @return Vector - The result of applying step-4a filtering
	 */
	private static Vector filter4a(Vector recordBuffer) {
		boolean isAfter = false;
		boolean isAfterTmp = false;
		Date recordEndDate = null;
		Date recordEndDateTmp = null;

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = new Date();
		//String debugTempId = null;

		try {
			for (int i = 0; i < recordBuffer.size(); i++) {
				String current = (String) recordBuffer.elementAt(i);

				/*recordEndDate = format.parse(parseRecord(current, 13));
				//debugTempId = parseRecord(current, 0);

				isAfter = recordEndDate.after(currentDate);
				if(debugTempId.equals("146094"))
					System.out.println("outside the 2nd loop: "+current);

				if(isAfter) {
					int index = 0;
					while ((recordBuffer.size() > 1)
						&& (index < recordBuffer.size())) {
						String tmpCurrent = (String) recordBuffer.elementAt(index);

						recordEndDateTmp = format.parse(parseRecord(tmpCurrent, 13));

						isAfterTmp = recordEndDateTmp.after(currentDate);

						if(debugTempId.equals("146094"))
							System.out.println("insde the 2nd loop: "+tmpCurrent);

						if (index == 0 && isAfterTmp) {
							index++;
							continue;
						}
						filter4a.append(tmpCurrent);

						recordBuffer.removeElementAt(index);
					}
				} else*/ if (parseRecord(current, 6).equals("265")) {

					int index = 0;
					while ((recordBuffer.size() > 1)
						&& (index < recordBuffer.size())) {
						String tmpCurrent = (String) recordBuffer.elementAt(index);
						recordEndDateTmp = format.parse(parseRecord(tmpCurrent, 11));

						isAfterTmp = recordEndDateTmp.after(currentDate);
						if (index == 0
							&& parseRecord(tmpCurrent, 6).equals("265") && isAfterTmp) {
							index++;
							continue;
						}
						filter4a.append(tmpCurrent);

						recordBuffer.removeElementAt(index);
					}
				}
			}
		} catch (Exception e) {
			System.out.println("DateFormatException in fullquery_employee.filter4a(): "+e);
		}

		return recordBuffer;
	}

	/**
	 * Performs step-4b filtering: "If all listings are from the same group type,
	 * then retain the listing with the lowest title value and delete all other
	 * listings for that employee"
	 * @param recordBuffer The Vector that potentially holds duplicates; what we are trying to clean
	 * @param typeCodes The Properties file to get the employee codes from
	 * @return Vector - The result of applying step-4b filtering
	 */
	private static Vector filter4b(Vector recordBuffer, Properties typeCodes) {
		int staffCount = 0, facultyCount = 0, postDocCount = 0, visitingCount = 0;

		boolean isAfterTmp = false;
		Date recordEndDateTmp = null;

		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		Date currentDate = new Date();
		String currentDateStr = format.format(currentDate), recordEndDateStr = null; 
		String smallestTitleCode = "99999999";
		Map<String, Integer>  idMap = new HashMap();
        ArrayList<String> idArray = new ArrayList();
		try {
			//---begin filtering: part4(b)
			if (recordBuffer.size() > 1) {
				for (int i = 0; i < recordBuffer.size(); i++) {
					String current = (String) recordBuffer.elementAt(i);
					idArray.add(parseRecord(current, 1));
					if (getStaffTitleCodes(typeCodes)
						.indexOf("'" + parseRecord(current, 4) + "'")
						> -1) {
						staffCount++;
					}

					if (getFacultyTitleCodes(typeCodes)
						.indexOf("'" + parseRecord(current, 4) + "'")
						> -1) {
						facultyCount++;
					}

					if (getPostDocTitleCodes(typeCodes)
						.indexOf("'" + parseRecord(current, 4) + "'")
						> -1) {
						postDocCount++;
					}
					
					if (getVisitingTitleCodes(typeCodes)
							.indexOf("'" + parseRecord(current, 4) + "'")
							> -1) {
						visitingCount++;
					}					
				}

		        for (int i = 0; i < idArray.size(); i++) {
		        	idMap.putIfAbsent(idArray.get(i), Collections.frequency(
		                                         idArray, idArray.get(i)));
		        }
		
				if (staffCount == recordBuffer.size()
					|| visitingCount == recordBuffer.size()
					|| facultyCount == recordBuffer.size()
					|| postDocCount == recordBuffer.size()) {
                    int strCompare = 0, tmpCount = 0;
                    String tmpCurrent = "";                 
					for (int j = 0; j < recordBuffer.size(); j++) {
						tmpCurrent = (String) recordBuffer.elementAt(j);
						smallestTitleCode = parseRecord(tmpCurrent, 4);
						if(idMap.containsKey(parseRecord(tmpCurrent, 4))) {
							tmpCount = idMap.get(parseRecord(tmpCurrent, 4));
							if(tmpCount == 1) {
								break;
							} else {
								tmpCount -= 1;
								tmpCurrent = (String) recordBuffer.elementAt(j);
							    strCompare = parseRecord(tmpCurrent, 4).compareTo(smallestTitleCode);
							    if(strCompare < 0 ) {
								    smallestTitleCode = parseRecord(tmpCurrent, 4);
							    }		
							}
						}						
					}
                    
					int index = 0;

					while ((recordBuffer.size() > 1)
						&& (index < recordBuffer.size())) {
						tmpCurrent = (String) recordBuffer.elementAt(index);
						recordEndDateTmp = format.parse(parseRecord(tmpCurrent, 11));
						recordEndDateStr = format.format(recordEndDateTmp);

						isAfterTmp = recordEndDateTmp.after(currentDate);

						if (index == 0
							&& parseRecord(tmpCurrent, 4).equals(smallestTitleCode) && StringUtils.isNumeric(parseRecord(tmpCurrent, 4)) && isAfterTmp) {

							index++;

							continue;
						}
						filter4b.append(tmpCurrent);
						recordBuffer.removeElementAt(index);
					}
				}
				smallestTitleCode = "99999999";
			}

	    }
		catch (Exception e) {
			System.out.println("DateFormatException in fullquery_employee.filter4b(): "+e);
		}
		//---end filtering: part 4(b)
		return recordBuffer;
	}

	/**
	 * Performs step-4c filtering: "If the listings are from different group
	 * types and a Faculty title exists, then retain the listing with the lowest
	 * Faculty title value and delete all other listings for that employee"
	 * @param recordBuffer The Vector that potentially holds duplicates; what we are trying to clean
	 * @param typeCodes The Properties file to get the employee codes from
	 * @return Vector - The result of applying step-4c filtering
	 */
	private static Vector filter4c(Vector recordBuffer, Properties typeCodes) {
		int staffCount = 0, facultyCount = 0, postDocCount = 0, visitingCount = 0;
		boolean isAfterTmp = false;
		Date recordEndDateTmp = null;

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = new Date();

		try {
			for (int i = 0; i < recordBuffer.size(); i++) {
				String current = (String) recordBuffer.elementAt(i);

				if (getStaffTitleCodes(typeCodes)
					.indexOf("'" + parseRecord(current, 4) + "'")
					> -1) {
					staffCount++;
				}

				if (getFacultyTitleCodes(typeCodes)
					.indexOf("'" + parseRecord(current, 4) + "'")
					> -1) {
					facultyCount++;
				}

				if (getPostDocTitleCodes(typeCodes)
					.indexOf("'" + parseRecord(current, 4) + "'")
					> -1) {
					postDocCount++;
				}
				
				if (getVisitingTitleCodes(typeCodes)
						.indexOf("'" + parseRecord(current, 4) + "'")
						> -1) {
					visitingCount++;
				}					
			}
            
			if ((facultyCount > 0) && ((staffCount > 0) || (visitingCount > 0) || (postDocCount > 0))) {
				String tmpCurrent = (String) recordBuffer.elementAt(0);
				String smallestTitleCode = parseRecord(tmpCurrent, 4);
				int strCompare = 0;
				for (int j = 1; j < recordBuffer.size(); j++) {
					tmpCurrent = (String) recordBuffer.elementAt(j);

					if (getFacultyTitleCodes(typeCodes)
						.indexOf("'" + parseRecord(tmpCurrent, 4) + "'")
						== -1) {
						//if title code is not in Faculty group, skip it
						continue;
					}
					strCompare = parseRecord(tmpCurrent, 4).compareTo(smallestTitleCode);
					if(strCompare < 0 ) {
						smallestTitleCode = parseRecord(tmpCurrent, 4);
					}
				}

				//note: smallestTitleCode should now contain the lowest
				//Faculty title value

				int index = 0;
				while ((recordBuffer.size() > 1)
					&& (index < recordBuffer.size())) {
					tmpCurrent = (String) recordBuffer.elementAt(index);
					recordEndDateTmp = format.parse(parseRecord(tmpCurrent, 11));

					isAfterTmp = recordEndDateTmp.after(currentDate);

					if (index == 0
						&& parseRecord(tmpCurrent, 4).equals(smallestTitleCode) && isAfterTmp) {
						index++;
						continue;
					}

					filter4c.append(tmpCurrent);
					recordBuffer.removeElementAt(index);
				}
			}
	    } catch (Exception e) {
			System.out.println("DateFormatException in fullquery_employee.filter4c(): "+e);
		}
		return recordBuffer;
	}

	/**
	 * Performs step-4d filtering: "If the listings are from the Post Doc and
	 * Staff groups, then retain the listing with the lowest Post Doc title
	 * value and delete all other listings for that employee"
	 * @param recordBuffer The Vector that potentially holds duplicates; what we are trying to clean
	 * @param typeCodes The Properties file to get the employee codes from
	 * @return Vector - The result of applying step-4d filtering
	 */
	private static Vector filter4d(Vector recordBuffer, Properties typeCodes) {
		int staffCount = 0, facultyCount = 0, postDocCount = 0, visitingCount = 0;
		boolean isAfterTmp = false;
		Date recordEndDateTmp = null;

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = new Date();

		try {
			for (int i = 0; i < recordBuffer.size(); i++) {
				String current = (String) recordBuffer.elementAt(i);

				if (getStaffTitleCodes(typeCodes)
					.indexOf("'" + parseRecord(current, 4) + "'")
					> -1) {
					staffCount++;
				}

				if (getFacultyTitleCodes(typeCodes)
					.indexOf("'" + parseRecord(current, 4) + "'")
					> -1) {
					facultyCount++;
				}

				if (getPostDocTitleCodes(typeCodes)
					.indexOf("'" + parseRecord(current, 4) + "'")
					> -1) {
					postDocCount++;
				}

				if (getVisitingTitleCodes(typeCodes)
						.indexOf("'" + parseRecord(current, 4) + "'")
						> -1) {
					visitingCount++;
				}				
			}

			if ((facultyCount == 0) && (staffCount > 0) && (postDocCount > 0) && (visitingCount > 0)) {
				String tmpCurrent = (String) recordBuffer.elementAt(0);
				String smallestTitleCode = parseRecord(tmpCurrent, 4);
				int strCompare = 0;
				for (int j = 1; j < recordBuffer.size(); j++) {
					tmpCurrent = (String) recordBuffer.elementAt(j);

					if (getPostDocTitleCodes(typeCodes)
						.indexOf("'" + parseRecord(tmpCurrent, 4) + "'")
						== -1) {
						//if title code is not in Post-doc group, skip it
						continue;
					}

					strCompare = parseRecord(tmpCurrent, 4).compareTo(smallestTitleCode);
					if(strCompare < 0 ) {
						smallestTitleCode = parseRecord(tmpCurrent, 4);
					}
				}

				//note: smallestTitleCode should now contain the lowest
				//Post-doc title value

				int index = 0;
				while ((recordBuffer.size() > 1)
					&& (index < recordBuffer.size())) {
					tmpCurrent = (String) recordBuffer.elementAt(index);
					recordEndDateTmp = format.parse(parseRecord(tmpCurrent, 11));

					isAfterTmp = recordEndDateTmp.after(currentDate);

					if (index == 0
						&& parseRecord(tmpCurrent, 4).equals(smallestTitleCode) && isAfterTmp) {
						index++;
						continue;
					}

					filter4d.append(tmpCurrent);
					recordBuffer.removeElementAt(index);
				}
			}
		} catch (Exception e) {
			System.out.println("DateFormatException in fullquery_employee.filter4d(): "+e);
		}
		return recordBuffer;
	}

	private static Vector removeStudentTitles(Vector recordBuffer, Properties typeCodes) {
		int index = 0;
		while ((recordBuffer.size() > 1)
			&& (index < recordBuffer.size())) {
			String current = (String) recordBuffer.elementAt(index);

			if (getStudentTitleCodes(typeCodes).indexOf("'" + parseRecord(current, 4) + "'") > -1) {
				//System.out.println("Removed: " + parseRecord(current, 1));
				recordBuffer.removeElementAt(index);
				continue;
			} else {
				index++;
			}
		}
		return recordBuffer;
	}

	/**
	 * Checks to see if the Patron record contains a student title-code
	 * @param typeCodes The properties taht define the title-codes
	 * @param in The patron record to check
	 * @return boolean true if patron has student title-code; else false
	 */
	private static boolean isStudentTitleCode(Properties typeCodes, String in) {

		if (getStudentTitleCodes(typeCodes)
			.indexOf("'" + parseRecord(in, 4) + "'")
			> -1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check wether or not a title code is valid. A title code is valid if
	 * the code is in the Properties file referenced by typeCodes
	 * @param typeCodes
	 * @param in
	 * @return
	 */
	private static boolean isValidCode(Properties typeCodes, String in) {
		boolean isValid = false;
		if (getStaffTitleCodes(typeCodes)
			.indexOf("'" + parseRecord(in, 4) + "'")
			> -1) {
			isValid = true;
		}
		if (getFacultyTitleCodes(typeCodes)
			.indexOf("'" + parseRecord(in, 4) + "'")
			> -1) {
			isValid = true;
		}
		if (getPostDocTitleCodes(typeCodes)
			.indexOf("'" + parseRecord(in, 4) + "'")
			> -1) {
			isValid = true;
		}
		if (getVisitingTitleCodes(typeCodes)
				.indexOf("'" + parseRecord(in, 4) + "'")
				> -1) {
				isValid = true;
			}		
		if (getStudentTitleCodes(typeCodes)
			.indexOf("'" + parseRecord(in, 4) + "'")
			> -1) {
			isValid = false;
		}

		return isValid;
	}

    private static boolean isValidEmpClassCode(String empClassCode, String titleCode) {
        boolean isValid = false;
        if(!EMP_STUDENT_CLASS_CODE.contains(empClassCode) ||
            (EMP_STUDENT_CLASS_CODE == empClassCode && NEW_STUDENT_STAFF_TITLE_CODES.contains(titleCode))
            || (titleCode != "4011") || (titleCode == "4011" && !EMP_STUDENT_CLASS_CODE.contains(empClassCode))) {
           isValid = true;
        }
        return isValid;
    }

    private static boolean isValidEmpStatus(String empStatusCode, String titleCode) {
      boolean isValid = false;
      if(EMPL_STATUS_CODE.contains(empStatusCode) || EMERITUS_TITLE_CODES.contains(titleCode) ) {
         isValid = true;
      }
      return isValid;
    }

	public static Vector loadMissingEmployees(String filePath) {
		BufferedReader in = null;
		Vector tmpVec = new Vector();
        try {
	        in = new BufferedReader(new FileReader(filePath + "emplInKavikNotAres2.txt"));
	        String lineIn = "";

	        while (((lineIn = in.readLine()) != null)
	              && !(lineIn.trim().equals(""))) {
	            lineIn = lineIn.trim();
	            if(!tmpVec.contains(lineIn)) {
	                tmpVec.add(lineIn);
	            }
	        }
	    } catch (IOException ioe) {
	        System.out.println(ioe);
	    }
        return tmpVec;
	}

	public static void getRawDataCsv(String props1, String props2, String props3, PrintWriter pw, boolean total, String idList) {
        if (!props3.equals("")) {
            props3 += File.separator;
        }

        // load the properties file to get the current quarter code
	    Properties typeCodes = null;
	    Properties original = null;
	    Properties affiliationCodes = null;
	    try {
	        original = FileUtils.loadProperties(props3);
	        typeCodes = FileUtils.loadProperties(props2);
	        affiliationCodes = FileUtils.loadProperties(props1);
	        getAllTypeCodes(typeCodes);
        } catch (IOException ioe) {
	        System.out.println("Error loading properties file!");
	        return;
        }

		Connection db2Conn = null;
		Map sysIdMap = null;

		String systemIdQuery = "select a.UCPATH_EMPLID, s.id from affiliates_dw.system s left join affiliates_dw.affiliates_safe_attributes a " +
						"on s.aid=a.aid where s.system_id=41 and UCPATH_EMPLID != 0 and s.id != ''";
		sysIdMap = Utility.loadSystemId(systemIdQuery);

	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	    String currentDate = format.format(new java.util.Date());
        String inputFile = props3.replace("patron_load.properties","") + "sourceDataTmp.csv";

        String empId = null, person_id = null, emp_name = null, title_code = null, title_name = null, dept_code = null;
        String dept_name = null, emp_status_code = null, mail_code = null, phone = null, email = null;
        String last_name = null, first_name = null, middle_name = null, uc_path_id = null, job_end_date = null;
        String barcode = "", pid = "", emp_student_status_code = null, employeeSystemId = null;
        String tmpStr = "", newId = "", oldId = "", job_indicator = "", tmp_uc_path_id = "", tmp_sys_id = "";
        StringBuffer resultBuffer = new StringBuffer();
        int validCount = 0, invalidCount = 0, studentTitleCodeCount = 0;
        Vector recordBuffer = new Vector(), tmpVec = new Vector();
        int counter = 1, noSysIdCounter = 0;
        boolean validStatCode = true, isEmpStatusValid = true;
        try {
        	String token = Utility.getToken(outputLocation, "system_id_token.txt", "getSystemIdToken.sh");
       	    Reader inputReader = new InputStreamReader(new FileInputStream(inputFile),"UTF-8");
        	BufferedReader reader = new BufferedReader(inputReader);        	
        	CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader().withIgnoreHeaderCase().withTrim());

        	for (CSVRecord csvRecord: csvParser) {
	            newId = csvRecord.get("Employee_PPS_ID_Current");
	            emp_name = csvRecord.get("Employee_Name_Current");
	            title_code = removeLeadingZeroes(csvRecord.get("Job_Code"));
	            title_name = csvRecord.get("Job_Code_Description");
	            dept_code = csvRecord.get("Department_Code");
	            dept_name = csvRecord.get("Department");
	            emp_status_code = csvRecord.get("Employee_Status_Code");
	            mail_code = csvRecord.get("Identity_Mail_Drop_Current");
	            phone = csvRecord.get("Employee_Work_Phone_Number_Current");
	            email = csvRecord.get("Employee_Work_Email_Address_Current");
	            if((email == null || StringUtils.isBlank(email)) || (email != null && !email.endsWith("@ucsd.edu"))) {
	                email = csvRecord.get("Identity_Email_Address_Current");
	            }
	            last_name = csvRecord.get("Employee_Last_Name_Current");
	            first_name = csvRecord.get("Employee_First_Name_Current");
	            middle_name = csvRecord.get("Employee_Middle_Name_Current");
	            uc_path_id = csvRecord.get("Employee_ID").trim();
	            job_end_date = csvRecord.get("Job_Expected_End_Date");
	            emp_student_status_code = csvRecord.get("Employee_Class_Code");
	            barcode = csvRecord.get("Identity_Bar_Code_Current");
	            pid = csvRecord.get("Identity_Student_PID_Current");
	            job_indicator = csvRecord.get("Job_Indicator");
	            phone = phone.replace("/", "").replace("-", "");	
	            if(sysIdMap.containsKey(uc_path_id)) {
	                tmp_sys_id = (String)sysIdMap.get(uc_path_id.trim());
	            }
	            isEmpStatusValid = true;
                if(total == false) {
                	isEmpStatusValid = isValidEmpStatus(emp_status_code,title_code);
                }

	            if(!tmpVec.contains(uc_path_id.trim()) && job_indicator.trim().equals("Primary") && isEmpStatusValid && 
	            		(idList == null || (!StringUtils.isBlank(email) && idList.contains(","+email.trim())) || idList.contains(uc_path_id.trim()) || idList.contains(tmp_sys_id.trim()))) {
		        	tmpVec.add(uc_path_id.trim());

		            if (job_end_date.length() == 0) {
		            	job_end_date = currentDate;
		            }

		            if (!uc_path_id.equals(oldId)) {
		                //ok, now we've come to a new ID, so write
		                //the old ID if it was unique; otherwise,
		                //process multiple until we only have one

		                //perform filter operations here

	                    recordBuffer = filter3a(recordBuffer, typeCodes);
		                recordBuffer = filter4a(recordBuffer);
		                recordBuffer = filter4b(recordBuffer, typeCodes);
		                recordBuffer = filter4c(recordBuffer, typeCodes);
		                recordBuffer = filter4d(recordBuffer, typeCodes);

		                if (recordBuffer.size() < 2) {
		                    if (recordBuffer.size() > 0) {

		                            //if we only have 1 record (removed all duplicates), do stuff here

		                        String tmp = (String) recordBuffer.elementAt(0);
		                        StringBuffer writeOut = new StringBuffer();
		                        empId = (String)parseRecord(tmp, 0);

		                        //write out the employee ID
		    	                if(parseRecord(tmp, 0) != null && parseRecord(tmp, 0).length() > 0)
		    	                    writeOut.append(parseRecord(tmp, 0, false)+"\t");
		    	                else
		    	                    writeOut.append("none\t");

		                        //write out the employee name
		    	                if(parseRecord(tmp, 1) != null && parseRecord(tmp, 1).length() > 0)
		    	                    writeOut.append(parseRecord(tmp, 1, false)+"\t");
		    	                else
		    	                    writeOut.append("none\t");

		                        //write out the employment status code:
		                        //--if emeritus, write out "A" for active
		                        if (isEmeritusTitleCode(parseRecord(tmp, 4))) {
		                            writeOut.append("A\t");
		                        } else {
		                            writeOut.append(parseRecord(tmp, 2) + "\t");
		                        }

		                        //write out the student status code
		    	                if(parseRecord(tmp, 3) != null && parseRecord(tmp, 3).length() > 0)
		    	                    writeOut.append(parseRecord(tmp, 3, false)+"\t");
		    	                else
		    	                    writeOut.append("none\t");

		                        //write out the patron-type code (mapped from title codes)
		                        if (typeCodes.get(parseRecord(tmp, 4)) != null) {

		                            //exeption: if department code=265 (affiliation code=29),
		                            //then assign patron type=16
		                            if (parseRecord(tmp, 6).equals("265")) {
		                                writeOut.append("16\t");
		                            } else {
		                                //if department_code != 265, then do this:

		                                if (typeCodes.get(parseRecord(tmp, 4)) == null) {
		                                    typeCodeNotFound.append(tmp);
		                                    writeOut.append("0\t");
		                                } else {
		                                    writeOut.append(typeCodes.get(parseRecord(tmp, 4)) + "\t");
		                                }
		                            }
		                        } else {
		                            //write a "0" if title code is not found in the mappings.
		                            writeOut.append("0" + "\t");
		                            titleCodeNotFound.append(tmp);
		                        }

		                        //write out the title name
		    	                if(parseRecord(tmp, 5) != null && parseRecord(tmp, 5).length() > 0)
		    	                    writeOut.append(parseRecord(tmp, 5, false)+"\t");
		    	                else
		    	                    writeOut.append("none\t");

		                        //write the affiliation code (mapped from department-code)
		                        if (affiliationCodes.get(parseRecord(tmp, 6)) != null) {
		                            writeOut.append(affiliationCodes.get(parseRecord(tmp, 6)) + "\t");
		                        } else {
		                            writeOut.append("0" + "\t");
		                            affiliationCodeNotFound.append(tmp);
		                        }

		                        //write out the department name
		    	                if(parseRecord(tmp, 7) != null && parseRecord(tmp, 7).length() > 0)
		    	                    writeOut.append(parseRecord(tmp, 7, false)+"\t");
		    	                else
		    	                    writeOut.append("none\t");

		                        //write out the mailcode
		                        if(parseRecord(tmp, 8) != null && parseRecord(tmp, 8).length() > 0)
		                            writeOut.append(parseRecord(tmp, 8, false)+"\t");
		                        else
		                            writeOut.append("none\t");

	                            //write out the phone
	                            if(parseRecord(tmp, 9) != null && parseRecord(tmp, 9).length() > 0)
	                                writeOut.append(parseRecord(tmp, 9, false)+"\t");
	                            else
	                                writeOut.append("none\t");

	                            //write out the email
	                            if(parseRecord(tmp, 10) != null && parseRecord(tmp, 10).length() > 0)
	                                writeOut.append(parseRecord(tmp, 10, false)+"\t");
	                            else
	                                writeOut.append("none\t");

	                            //write out the barcode
	                            if(parseRecord(tmp, 15) != null && parseRecord(tmp, 15).length() > 0)
	                                writeOut.append(parseRecord(tmp, 15, false)+"\t");
	                            else
	                                writeOut.append("none\t");

	                            //write out the PID
	                            if(parseRecord(tmp, 16) != null && parseRecord(tmp, 16).length() > 0)
	                                writeOut.append(parseRecord(tmp, 16, false)+"\t");
	                            else
	                                writeOut.append("none\t");

	                            //write out the UCPathID
	                            if(parseRecord(tmp, 17) != null && parseRecord(tmp, 17).length() > 0) {
	                                writeOut.append(parseRecord(tmp, 17, false)+"\t");
	                                tmp_uc_path_id = parseRecord(tmp, 17, false);
	                            } else
	                                writeOut.append("none\t");

	                            //write out the systemId
	            	            if(sysIdMap.containsKey(tmp_uc_path_id)) {
	            	            	writeOut.append(sysIdMap.get(tmp_uc_path_id)+"\t");
	            	            } else {
	            	            	writeOut.append("none\t");
	                            }

	            	            //write out the last_name
	    	                    if(parseRecord(tmp, 12) != null && parseRecord(tmp, 12).length() > 0)
	    	                        writeOut.append(parseRecord(tmp, 12, false)+"\t");
	    	                    else
	    	                        writeOut.append("none\t");
	    	                    
	    	                    //write out the first_name
	    	                    if(parseRecord(tmp, 13) != null && parseRecord(tmp, 13).length() > 0)
	    	                        writeOut.append(parseRecord(tmp, 13, false)+"\t");
	    	                    else
	    	                        writeOut.append("none\t");

	    	                    //write out the middle_name
	    	                    if(parseRecord(tmp, 14) != null && parseRecord(tmp, 14).length() > 0)
	    	                        writeOut.append(parseRecord(tmp, 14, false)+"\t");
	    	                    else
	    	                        writeOut.append("none\t");

	    	                    //write out the dept_code
	    	                    if(parseRecord(tmp, 6) != null && parseRecord(tmp, 6).length() > 0)
	    	                        writeOut.append(parseRecord(tmp, 6, false)+"\t");
	    	                    else
	    	                        writeOut.append("none\t");

	    		                //write out the job end date
	    	                    if(parseRecord(tmp, 11) != null && parseRecord(tmp, 11).length() > 0)
	    	                        writeOut.append(parseRecord(tmp, 11, false));
	    	                    else
	    	                        writeOut.append("none");
	    	                    
		                        pw.write(writeOut.toString() + "\n");
		                        empId = null;
		                    }
		                } else {
		                    //if this is still a duplicate, dump it into another file
		                    for (int i = 0; i < recordBuffer.size(); i++) {
		                        moreDuplicates.append((String) recordBuffer.elementAt(0));
		                    }
		                }

		                //clear the Vector for use with new records
		                recordBuffer = new Vector();
		            }
		            oldId = new String(uc_path_id);

		            StringBuffer bufferEntry = new StringBuffer("");

		            bufferEntry.append(newId+"\t"+emp_name+"\t"+emp_status_code+"\t"+emp_student_status_code+"\t");
	                bufferEntry.append(title_code+"\t"+title_name+"\t"+dept_code+"\t");
	                bufferEntry.append(dept_name+"\t"+mail_code+"\t"+phone+"\t"+email+"\t");
	                bufferEntry.append(job_end_date+"\t"+last_name+"\t"+first_name+"\t");
	                bufferEntry.append(middle_name+"\t"+barcode+"\t"+pid+"\t"+uc_path_id+"\t\n");
	                
		            String record = bufferEntry.toString();

		            //--check if title code is valid
		            boolean isValid = isValidCode(typeCodes, record);

		            if(!isValid && tmpVec.contains(uc_path_id.trim())) {
		            	tmpVec.remove(uc_path_id.trim());
		            }
	                //--filter out records that are not staff, faculty, post-doc,
		            //or student-title codes (these get filtered out later)
	                /*isEmpStatusValid = true;
	                if(total == false) {
	                	isEmpStatusValid = isValidEmpStatus(emp_status_code,title_code);
	                	validStatCode = isEmpStatusValid;
	                }*/

		            boolean isClassCodeValid = isValidEmpClassCode(emp_student_status_code,title_code);
		            /*boolean isClassCodeValid = false;

		            if(emp_student_status_code.equals("13") || emp_student_status_code.equals("14")) { 
			            isClassCodeValid = isValidEmpClassCode(emp_student_status_code,title_code);			          
		            } else {
		            	isValid = false;
		            }*/
		            if (isValid && isEmpStatusValid && isClassCodeValid) {
		            	validCount++;
		                recordBuffer.addElement(record);
		             } else {
		            	invalidCount++;
		                //if it's invalid, write to file
		                //a record is invalid if title code does not fall in either:
		                //the staff, faculty, or post-doc groups. It is also invalid
		                //if code falls in the Student Position list of title codes.

		                //if it's a studentTitleCode, don't write it to file
		                if (!isStudentTitleCode(typeCodes, record)) {
		                	invalidRecords.append(record);
		                } else {
		                    studentTitleCodeCount++; 
		                }
		             }
		            //job_end_date = null;
	             }
	        	 }
		         //duplicate the code to handle the last row

		         recordBuffer = filter3a(recordBuffer, typeCodes);
		         recordBuffer = filter4a(recordBuffer);
		         recordBuffer = filter4b(recordBuffer, typeCodes);
		         recordBuffer = filter4c(recordBuffer, typeCodes);
		         recordBuffer = filter4d(recordBuffer, typeCodes);

		         if (recordBuffer.size() < 2) {
		            if (recordBuffer.size() > 0) {
		                //if we only have 1 record (removed all duplicates), do stuff here

		                String tmp = (String) recordBuffer.elementAt(0);
		                StringBuffer writeOut = new StringBuffer();
		                empId = (String)parseRecord(tmp, 0);
                       
		                //write out the employee ID
		                if(parseRecord(tmp, 0) != null && parseRecord(tmp, 0).length() > 0)
		                    writeOut.append(parseRecord(tmp, 0, false)+"\t");
		                else
		                    writeOut.append("none\t");

		                //write out the employee name
		                if(parseRecord(tmp, 1) != null && parseRecord(tmp, 1).length() > 0)
		                    writeOut.append(parseRecord(tmp, 1, false)+"\t");
		                else
		                    writeOut.append("none\t");

		                //write out the employment status code:
		                if (isEmeritusTitleCode(parseRecord(tmp, 4))) {
		                    writeOut.append("A\t");
		                } else {
		                    writeOut.append(parseRecord(tmp, 2) + "\t");
		                }

		                //write out the student status code
		                if(parseRecord(tmp, 3) != null && parseRecord(tmp, 3).length() > 0)
		                    writeOut.append(parseRecord(tmp, 3, false)+"\t");
		                else
		                    writeOut.append("none\t");

		                //write out the patron-type code (mapped from title codes)
		                if (typeCodes.get(parseRecord(tmp, 4)) != null) {

		                    //exeption: if department code=265 (affiliation code=29),
		                    //then assign patron type=16
		                    if (parseRecord(tmp, 6).equals("265")) {
		                        writeOut.append("16\t");
		                    } else {
		                        //if department_code != 265, then do this:

		                        if (typeCodes.get(parseRecord(tmp, 4)) == null) {
		                                typeCodeNotFound.append(tmp);
		                                writeOut.append("0\t");
		                        } else {
		                                writeOut.append(
		                                    typeCodes.get(parseRecord(tmp, 4)) + "\t");
		                        }
		                    }
		                } else {
		                    //write a "0" if title code is not found in the mappings.
		                    writeOut.append("0" + "\t");
		                    titleCodeNotFound.append(tmp);
		                }

		                //write out the title name
		                if(parseRecord(tmp, 5) != null && parseRecord(tmp, 5).length() > 0)
		                    writeOut.append(parseRecord(tmp, 5, false)+"\t");
		                else
		                    writeOut.append("none\t");

		                //write the affiliation code (mapped from department-code)
		                if (affiliationCodes.get(parseRecord(tmp, 6)) != null) {
		                    writeOut.append(affiliationCodes.get(parseRecord(tmp, 6)) + "\t");
		                } else {
		                    writeOut.append("0" + "\t");
		                    affiliationCodeNotFound.append(tmp);
		                }

		                //write out the department name
		                if(parseRecord(tmp, 7) != null && parseRecord(tmp, 7).length() > 0)
		                    writeOut.append(parseRecord(tmp, 7, false)+"\t");
		                else
		                    writeOut.append("none\t");

		                //write out the mailcode
		                if(parseRecord(tmp, 8) != null && parseRecord(tmp, 8).length() > 0)
		                    writeOut.append(parseRecord(tmp, 8, false)+"\t");
		                else
		                    writeOut.append("none\t");

		                //write out the phone
	                    if(parseRecord(tmp, 9) != null && parseRecord(tmp, 9).length() > 0)
	                        writeOut.append(parseRecord(tmp, 9, false)+"\t");
	                    else
	                        writeOut.append("none\t");

		                //write out the email
	                    if(parseRecord(tmp, 10) != null && parseRecord(tmp, 10).length() > 0)
	                        writeOut.append(parseRecord(tmp, 10, false)+"\t");
	                    else
	                        writeOut.append("none\t");

	                    //write out the barcode
	                    if(parseRecord(tmp, 15) != null && parseRecord(tmp, 15).length() > 0)
	                        writeOut.append(parseRecord(tmp, 15, false)+"\t");
	                    else
	                        writeOut.append("none\t");

	                    //write out the pid
	                    if(parseRecord(tmp, 16) != null && parseRecord(tmp, 16).length() > 0)
	                        writeOut.append(parseRecord(tmp, 16, false)+"\t");
	                    else
	                        writeOut.append("none\t");

	                    //write out the UCPathId
	                    if(parseRecord(tmp, 17) != null && parseRecord(tmp, 17).length() > 0) {
	                        writeOut.append(parseRecord(tmp, 17, false)+"\t");
	                        tmp_uc_path_id = parseRecord(tmp, 17, false).trim();
	                    } else
	                        writeOut.append("none\t");

                        //write out the systemId
        	            if(sysIdMap.containsKey(tmp_uc_path_id)) {
        	            	writeOut.append(sysIdMap.get(tmp_uc_path_id)+"\t");
                        } else {
        	            	employeeSystemId = Utility.systemID(tmp_uc_path_id, "ucpath_emplid", token);
        				    if(employeeSystemId.equals("null")) {
        				        System.out.println("No sysId for employee:"+tmp_uc_path_id);
        				        Utility.postSystemID(tmp_uc_path_id, "ucpath_emplid", token);
        				        employeeSystemId = Utility.systemID(tmp_uc_path_id, "ucpath_emplid", token);
        				        if(employeeSystemId.equals("null")) {
        				        	System.out.println("$$$$ No sysId for employee:"+tmp_uc_path_id);
        				        }
        				    }
        				    //Utility.delay(1, TimeUnit.SECONDS);	
        				    writeOut.append(employeeSystemId+"\t");
        				}

        	            //write out the last_name
	                    if(parseRecord(tmp, 12) != null && parseRecord(tmp, 12).length() > 0)
	                        writeOut.append(parseRecord(tmp, 12, false)+"\t");
	                    else
	                        writeOut.append("none\t");
	                    
	                    //write out the first_name
	                    if(parseRecord(tmp, 13) != null && parseRecord(tmp, 13).length() > 0)
	                        writeOut.append(parseRecord(tmp, 13, false)+"\t");
	                    else
	                        writeOut.append("none\t");

	                    //write out the middle_name
	                    if(parseRecord(tmp, 14) != null && parseRecord(tmp, 14).length() > 0)
	                        writeOut.append(parseRecord(tmp, 14, false)+"\t");
	                    else
	                        writeOut.append("none\t");

	                    //write out the dept_code
	                    if(parseRecord(tmp, 6) != null && parseRecord(tmp, 6).length() > 0)
	                        writeOut.append(parseRecord(tmp, 6, false)+"\t");
	                    else
	                        writeOut.append("none\t");

		                //write out the job end date
	                    if(parseRecord(tmp, 11) != null && parseRecord(tmp, 11).length() > 0)
	                        writeOut.append(parseRecord(tmp, 11, false));
	                    else
	                        writeOut.append("none");
	                    
		                pw.write(writeOut.toString() + "\n");
		                empId = null;
		            }
		        } else {
		            //if this is still a duplicate, dump it into another file
		            for (int i = 0; i < recordBuffer.size(); i++) {
		                moreDuplicates.append((String) recordBuffer.elementAt(0));
		            }
		        }
	        //}

        } catch (Exception ex) {
	        System.err.println("Fullquery_empl Exception: " + ex);
	        ex.printStackTrace();
	    }
	}

    public static String removeLeadingZeroes(String str) {
        String strPattern = "^0+(?!$)";
        str = str.replaceAll(strPattern, "");
        return str;
    }

	private static void getAllTypeCodes(Properties props) {
		StringBuffer tmpStringBuffer = new StringBuffer();

		for (Enumeration e = props.propertyNames(); e.hasMoreElements();) {
			String value = (String) e.nextElement();
			if(value != null)
				tmpStringBuffer.append("'"+value+"',");
		}
		NEW_STUDENT_STAFF_TITLE_CODES = tmpStringBuffer.toString();
		NEW_STUDENT_STAFF_TITLE_CODES = NEW_STUDENT_STAFF_TITLE_CODES.substring(0, NEW_STUDENT_STAFF_TITLE_CODES.length()-1);
	}
	private static Vector recordBuffer;
	private static String outputLocation;
	private static String PATHNAME;

	private static StringBuffer filter3a = new StringBuffer();
	private static StringBuffer filter4a = new StringBuffer();
	private static StringBuffer filter4b = new StringBuffer();
	private static StringBuffer filter4c = new StringBuffer();
	private static StringBuffer filter4d = new StringBuffer();
	private static StringBuffer titleCodeNotFound = new StringBuffer();
	private static StringBuffer typeCodeNotFound = new StringBuffer();
	private static StringBuffer affiliationCodeNotFound = new StringBuffer();
	private static StringBuffer moreDuplicates = new StringBuffer();
	private static StringBuffer invalidRecords = new StringBuffer();

	public static final String STUDENT_STAFF_TITLE_CODES = "'771', '772', '843', '1506', '1630', '1631', '1728', '2077', '2220', '2221', '2709', '2723', '2724', '2725', '2728','2729', '2738', '3220', '3394', '3240', '3296'";

    public static final String EMERITUS_TITLE_CODES = "'1132', '1620', '1621', '3249', '3800'";
	public static final String EMP_STUDENT_STATUS_CODE = "'1', '3'";
	public static final String SEPARATED_CODES = "'S', 'K'";
	public static final String BLOCKED_STATUS_CODES = "'K'";
	public static String NEW_STUDENT_STAFF_TITLE_CODES = "";
    public static final String EMP_STUDENT_CLASS_CODE = "5,13,14";
    public static final String EMPL_STATUS_CODE = "A,L,P,W";
}


/*
This is a sample query that can be used for debugging purposes:

----------------FULL--------------------
SELECT DISTINCT
p.emb_person_id EMPID,
p.emb_employee_name NAME,
p.emp_employment_status_code,
p.emp_student_status_code,
p1.app_title_code TITLE_CODE,
p1.app_title_name,
p1.app_department_code DEPT_CODE,
p1.app_department_name,
p.emp_student_status_code,
ph.employee_mail_code MAIL_CODE,
ph.employee_office_phone PHONE,
ph.employee_email EMAIL
FROM
dbo.p_employee p,
dbo.p_appointment p1,
dbo.p_distribution p2,
phone.dbo.employee ph
WHERE
ph.emb_employee_number = p.emb_person_id AND (
((p.emp_student_status_code = '1') or
(p.emp_student_status_code = '3')) AND
(p1.emb_id = p.emb_id) AND
(p2.emb_id = p.emb_id) AND
(p2.app_appointment_number = p1.app_appointment_number)
)
ORDER BY
EMPID, p.emb_employee_name


--------FULL ACTIVE---------------
SELECT DISTINCT
p.emb_person_id EMPID,
p.emb_employee_name NAME,
p.emp_employment_status_code,
p.emp_student_status_code,
p1.app_title_code TITLE_CODE,
p1.app_title_name,
p1.app_department_code DEPT_CODE,
p1.app_department_name,
p.emp_student_status_code
FROM
dbo.p_employee p,
dbo.p_appointment p1,
dbo.p_distribution p2
WHERE

((p.emp_student_status_code IN ('1', '3')) OR
((p.emp_student_status_code = '4') AND (p1.app_title_code IN ('771', '772', '843', '1506', '1630', '1631', '1728', '2077', '2220', '2221', '2709', '2723', '2724', '2725', '2728', '2738', '3220', '3394', '3240', '3296')) ))


AND ((p1.app_title_code <> '4011') OR ((p1.app_title_code = '4011') AND (p.emp_student_status_code = '1')))


AND (p1.emb_id = p.emb_id)
AND (p2.emb_id = p.emb_id)
AND (p2.app_appointment_number = p1.app_appointment_number)

AND ((p.emp_employment_status_code NOT IN ('S', 'I', 'K')) OR
(p1.app_title_code IN ('1132', '1620', '1621', '3249', '3800')))

and p.emb_person_id=370707

ORDER BY
EMPID, p.emb_employee_name

----
select * from dbo.p_employee where emb_employee_name like 'RIGOLI%'
*/







