package edu.ucsd.library.patronload.apps;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys;
import edu.ucsd.library.util.FileUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.TimeUnit;

public class XmlUtility {
	public static final String EMPLOYEE = "employee";
	public static final String STUDENT = "student";
	public static final String GRAD_GROUP = "2";
	public static final int HOME_ADDRESS_INDEX = 7;
	public static final int SCHOOL_ADDRESS_INDEX = 9;
	public static final int ADDRESS_LINES_NUMBER = 5;
	public static final int EMPLOYEE_BARCODE_INDEX = 11;
	public static final int EMPLOYEE_UNIV_ID_INDEX = 12;
	public static final int EMPLOYEE_ID_2_INDEX = 13;
	public static final int EMPLOYEE_SYS_ID_INDEX = 14;
	public static final int EMPLOYEE_PHONE_INDEX = 9;
	public static final int EMPLOYEE_EMAIL_INDEX = 10;
	public static final int EMPLOYEE_MAILCODE_INDEX = 8;
	public static final int STUDENT_BARCODE_INDEX = 12;
	public static final int STUDENT_UNIV_ID_INDEX = 0;
	public static final int STUDENT_SYS_ID_INDEX = 13;
	public static final int STUDENT_EMAIL_INDEX = 11;
	public static final int STUDENT_PHONE_INDEX = 8;
	
	public static Document createDoc() {
		Document doc = null;
		try {
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
   	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
   	        doc = dBuilder.newDocument();
		} catch (Exception e) {
			e.printStackTrace();
		}
   	    return doc;   	    
	}
	
	public static void writeXmlToFile(Document doc, String fileName) {
		PrintWriter pw = null;
		try {
			OutputStream os = new FileOutputStream(fileName);
		    TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
		    StreamResult result = new StreamResult(new StringWriter());
		    transformer.transform(source, result);
		    pw = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
		    pw.write(result.getWriter().toString());
		} catch (Exception e) {
        	e.printStackTrace();
        } finally {
			if (pw != null)
				pw.close();
        }
	}

	public static void unescapeXml(String fileName) {
		BufferedReader reader = null;
		PrintWriter writer = null;
		try {
			Reader inputReader = new InputStreamReader(new FileInputStream(fileName),"UTF-8");
			reader = new BufferedReader(inputReader);

			String lineIn = "";			
			OutputStream os = new FileOutputStream(fileName.replace("-temp.xml",".xml"));
			writer = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));
			
			while (((lineIn = reader.readLine()) != null)
	              && !(lineIn.trim().equals(""))) {
	              writer.write(lineIn.trim().replaceAll("&amp;amp;#","&#").replaceAll("&amp;apos;","&apos;").replaceAll("&amp;quot;","&quot;").replaceAll("&amp;lt;","&lt;").replaceAll("&amp;gt;","&gt;").replaceAll("&amp;amp;","&amp;"));  
			}
		} catch (Exception e) {
			System.err.println("Error in unescapeXml:"+e.getMessage());
        	e.printStackTrace();
        } finally {
        	try {
	        	if(reader != null)
	        		reader.close();
	        	if(writer != null)
	        		writer.close();
        	} catch (Exception e) {}
        }
	}

	
	public static void writeData(Document doc, Element rootElement, Vector buildElementsVector) {
		try {
			Element parent = doc.createElement("user");
            rootElement.appendChild(parent);
            for (int i = 0; i < buildElementsVector.size(); i++) {
            	parent.appendChild((Element)buildElementsVector.get(i));
            }
		} catch (Exception e) {
        	e.printStackTrace();
        }
	}
	
	public static Vector buildElements(Document doc, Map elementMap) {
		String key = "", value = "";
		Vector elementVec = null;
		try {			
			elementVec = new Vector();
			if(elementMap != null) {
	        	for (Iterator i = elementMap.keySet().iterator(); i.hasNext();) {
				    key = (String) i.next();
					value = elementMap.get(key).toString();
					if (!value.equals("none"))
					    elementVec.add(buildElement(doc, key, value));
				}
            }
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return elementVec;
    }

	public static String convertValue (String in) {
		String value = in.trim();
		if (value.startsWith("none")) {
			value = "";
		}
		return value;
	}

	public static String userGroupCode (String in) {
		String value = in.trim();
		if (value.equals("16")) {
			value = "LIBSTAFF";
		} else if (value.equals("1")) {
			value = "UCACADEMIC";
		} else if (value.equals("17")) {
			value = "UCSTAFF";
		} else if (value.equals("40")) {
			value = "POSTDOC";
		} else if (value.equals("3")) {
			value = "UCUNDERGRAD";
		} else if (value.equals("2")) {
			value = "UCGRADSTUDENT";
		} else if (value.equals("41")) {
			value = "VISSCHL";
		} else if (value.equals("42")) {
			value = "VISGRAD";
		}
		return value;
	}

	public static String expireDate (String titleCode, String endDate) {
		String expiredate = "";
		String marcFilesDir = null;
	    try {
	        InitialContext jndi = new InitialContext();
	        marcFilesDir = (String)jndi.lookup("java:comp/env/marcFilePath");
	    } catch (NamingException e) {
	        e.printStackTrace();
	    }
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	    String currentDate = format.format(new java.util.Date());

		Properties patronLoadProperties = makemarc.getPatronLoadProperties(marcFilesDir);
		expiredate = endDate;
		if (titleCode.equals("1") || titleCode.equals("16") || titleCode.equals("40")) {		
		    expiredate = convertDate((String) patronLoadProperties.get("expiredate_employee1"));
		} else if (titleCode.equals("41") || titleCode.equals("42")) {
			if (currentDate.equals(endDate)) {
			    expiredate = convertDate((String) patronLoadProperties.get("expiredate_undergrad"));
			}
		} else {
			expiredate = convertDate((String) patronLoadProperties.get("expiredate_employee2"));
		}
		return expiredate;
	}

	public static String expireDateStudent (String level) {
		String expiredate = "";
		String marcFilesDir = null;
	    try {
	        InitialContext jndi = new InitialContext();
	        marcFilesDir = (String)jndi.lookup("java:comp/env/marcFilePath");
	    } catch (NamingException e) {
	        e.printStackTrace();
	    }
		Properties patronLoadProperties = makemarc.getPatronLoadProperties(marcFilesDir);
		if (level.equals("3")) {
			// undergraduate is level 3
			expiredate =
				(String) patronLoadProperties.get(
					"expiredate_undergrad");
		}

		if (level.equals("2")) {
			// graduate, medical, and PH is level 2
			expiredate =
				(String) patronLoadProperties.get(
					"expiredate_graduate");
		}

		expiredate = expiredate.trim();
		return convertDate(expiredate);
	}
	
	public static Element buildElement(Document doc, String key, String value) {
		Element tmpElement = null;
		try {	
			tmpElement = doc.createElement(key);
			tmpElement.appendChild(doc.createTextNode(value));
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return tmpElement;
    }
	
	public static void buildAttribute(Document doc, Element parent, Map attributeMap) {
		Attr attrType = null;
		String key = "", value = "";
		try {	
			if (attributeMap != null) {
				for (Iterator i = attributeMap.keySet().iterator(); i.hasNext();) {
				    key = (String) i.next();
					value = attributeMap.get(key).toString();
					attrType = doc.createAttribute(key);
			        attrType.setValue(value);
			        parent.setAttributeNode(attrType);
				}
			}
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
    }
	
	public static Element emailBlock (Document doc, String parentName, String lineIn, boolean preferred, String type) {
		Element email = null;
		Map tmpMap = null;
		try {	
			email = doc.createElement(parentName);			
			tmpMap = new LinkedHashMap();
			tmpMap.put("preferred", Boolean.toString(preferred));
	        tmpMap.put("segment_type", "External");
	        buildAttribute(doc, email, tmpMap);
	        String[] strArray = lineIn.split("\t");	        
	        String emailAddress = "", tmpEmail = "";
	        if(type.equals("work"))
	            emailAddress = convertValue(strArray[10]);
	        else {
				tmpEmail = strArray[11];
				if((tmpEmail == null || StringUtils.isBlank(tmpEmail) || !tmpEmail.endsWith("@ucsd.edu")) && fullquery.studentEmailMap.containsKey(strArray[0])) {
					tmpEmail = fullquery.studentEmailMap.get(strArray[0]).toString();
				}			
	        	emailAddress = convertValue(tmpEmail);	        	
	        }
	        tmpMap = new LinkedHashMap();
	        tmpMap.put("email_address", emailAddress);
	        
	        Vector tmpVec = buildElements(doc, tmpMap);
	        Element tmp = null;
	        for(int i = 0; i < tmpVec.size(); i++) {
	            tmp = (Element)tmpVec.get(i);
	        	email.appendChild(tmp);
	        }
	         
	        tmp = doc.createElement("email_types");
	        Element child = buildElement(doc, "email_type", type);
	        tmp.appendChild(child);
	        email.appendChild(tmp);
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return email;		
	}
	
	public static Element addressBlock (Document doc, String parentName, String lineIn, boolean preferred, String type) {
		Element address = null;
		Map tmpMap = null;
		try {	
			address = doc.createElement(parentName);			
			tmpMap = new LinkedHashMap();
			tmpMap.put("preferred", Boolean.toString(preferred));
	        tmpMap.put("segment_type", "External");
	        buildAttribute(doc, address, tmpMap);
	        
	        String[] strArray = lineIn.split("\t"), addressArray = null;
			String mailCode = "", addressLine1 = "", city = "", zipCode = "", state = "";
			tmpMap = new LinkedHashMap();
			int index = SCHOOL_ADDRESS_INDEX;
			if(type.equals("home")) {
				index = HOME_ADDRESS_INDEX;
			}
			
			if(type.equals("work")) {
		        mailCode = convertValue(strArray[8]);
		        tmpMap.put("line1", mailCode);
			} else {				
				addressArray = parseAddress(strArray[index], "line").split("\\$");
				for(int i = 0, j = 1; i < ADDRESS_LINES_NUMBER; i++, j++) {
					if(j <= addressArray.length)
						tmpMap.put("line"+j, StringEscapeUtils.escapeXml10(addressArray[i]));
					else 
						tmpMap.put("line"+j, "");
				}
				
				city = parseAddress(strArray[index], "city").replace("$","");
				state = parseAddress(strArray[index], "state");
				zipCode = parseAddress(strArray[index], "zipcode");
		    }
	        tmpMap.put("city", city);
	        tmpMap.put("state_province", state);
	        tmpMap.put("postal_code", zipCode);
	        tmpMap.put("address_note", "");
	        tmpMap.put("start_date", "");
	         
	        Vector tmpVec = buildElements(doc, tmpMap);
	        Element tmp = null;
	        for(int i = 0; i < tmpVec.size(); i++) {
	            tmp = (Element)tmpVec.get(i);
	        	address.appendChild(tmp);
	        }
	         
	        tmp = doc.createElement("address_types");
	        Element child = buildElement(doc, "address_type", type);
	        tmp.appendChild(child);
	        address.appendChild(tmp);
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return address;		
	}
	
	public static Element phoneBlock (Document doc, String parentName, String lineIn, boolean preferred, String type) {
		Element phone = null;
		Map tmpMap = null;
		try {	
			phone = doc.createElement(parentName);			
			tmpMap = new LinkedHashMap();
			tmpMap.put("preferred", Boolean.toString(preferred));
			tmpMap.put("preferred_sms", "false");
	        tmpMap.put("segment_type", "External");
			buildAttribute(doc, phone, tmpMap);
			String[] strArray = lineIn.split("\t");
		    String phoneNumber = "";
		    if(type.equals("office"))
		        phoneNumber = convertValue(strArray[9]);
		    else
		    	phoneNumber = convertValue(strArray[8]);
	        tmpMap = new LinkedHashMap();
	        tmpMap.put("phone_number", phoneNumber);
	        
	        Vector tmpVec = XmlUtility.buildElements(doc, tmpMap);
	        Element tmp = null;
	        for(int i = 0; i < tmpVec.size(); i++) {
	            tmp = (Element)tmpVec.get(i);
	        	phone.appendChild(tmp);
	        }
	         
	        tmp = doc.createElement("phone_types");
	        Element child = buildElement(doc, "phone_type", type);
	        tmp.appendChild(child);
	        phone.appendChild(tmp);
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return phone;		
	}
	
	public static Element userIdBlock (Document doc, String parentName, String lineIn, String type, String userType, String sysIdToken) {
		Element userId = null;
		Map tmpMap = null;
		try {	
			userId = doc.createElement(parentName);			
			tmpMap = new LinkedHashMap();
	        tmpMap.put("segment_type", "External");
			buildAttribute(doc, userId, tmpMap);
	        		    
	        tmpMap = new LinkedHashMap();
	        tmpMap.put("id_type", type);
	        tmpMap.put("value", idValue(userType, type, lineIn, sysIdToken));
	        tmpMap.put("status", "ACTIVE");
	       
	        Vector tmpVec = buildElements(doc, tmpMap);
	        Element tmp = null;
	        for(int i = 0; i < tmpVec.size(); i++) {
	            tmp = (Element)tmpVec.get(i);
	            userId.appendChild(tmp);
	        }

	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return userId;		
	}

	public static String pidValue(String data) {
		String[] strArray = data.split("\t");
	    String value = convertValue(strArray[EMPLOYEE_UNIV_ID_INDEX]);
		return value.toLowerCase();
	}
	
	public static String idValue(String userType, String idType, String data, String sysIdToken) {
		String[] strArray = data.split("\t");
	    String value = "", sysId = "", tmpSysId = "", reqType = "pid" ;
	    int index = EMPLOYEE_BARCODE_INDEX, univId = 0;
	    if(userType.equals(EMPLOYEE)) {
	    	reqType = "ucpath_emplid";
	    	if(idType.equals("BARCODE"))
	    		index = EMPLOYEE_BARCODE_INDEX;
		    else if(idType.equals("UNIV_ID")) {
		    	index = EMPLOYEE_UNIV_ID_INDEX;
		    	univId = index;
		    } else if(idType.equals("OTHER_ID_2"))
		    	index = EMPLOYEE_ID_2_INDEX;
		    else if(idType.equals("OTHER_ID_4")) {
		    	index = EMPLOYEE_SYS_ID_INDEX;
		    	univId = EMPLOYEE_ID_2_INDEX;
		    	sysId = "@ucsd.edu";
		    }
	    } else {
	    	if(idType.equals("BARCODE"))
	    		index = STUDENT_BARCODE_INDEX;
		    else if(idType.equals("UNIV_ID")) {
		    	index = STUDENT_UNIV_ID_INDEX;
		    	univId = index;
		    }
		    else if(idType.equals("OTHER_ID_4")) {
		    	index = STUDENT_SYS_ID_INDEX;
		    	univId = STUDENT_UNIV_ID_INDEX;
		    	sysId = "@ucsd.edu";
		    }
	    }
	    /*
	    if(idType.equals("OTHER_ID_4") && convertValue(strArray[index]).equals("")) {
	    	tmpSysId = Utility.systemID(convertValue(strArray[univId]), reqType, sysIdToken);
	        if (tmpSysId == null || tmpSysId.equals("null")) {
	        	Utility.delay(3, TimeUnit.SECONDS);
	    	    Utility.postSystemID(convertValue(strArray[univId]), reqType, sysIdToken);
	    	    Utility.delay(3, TimeUnit.SECONDS);
	    	    if(!NEW_PATRON_SYS_ID_LIST.contains(strArray[univId])) {
	    	        NEW_PATRON_SYS_ID_LIST.add(strArray[univId]);
	    	    }
	    	    tmpSysId = Utility.systemID(convertValue(strArray[univId]), reqType, sysIdToken);
	    	    if (tmpSysId == null ||tmpSysId.equals("null")) {
	    	    	tmpSysId = "";
	    	    }
	        } else {
	    	    Utility.delay(3, TimeUnit.SECONDS);
	        }
	        value = tmpSysId+sysId;
	    } else {
	        value = convertValue(strArray[index])+sysId;
	    }*/
	    value = convertValue(strArray[index])+sysId;
		return value;
	}

	public static Element statsBlock (Document doc, String parentName, String data, Map statsMap, String userType) {
		Element stat = null;
		Map tmpMap = null;
		try {	
			String[] strArray = data.split("\t");
		    String category = "";
		    int index = 18;
		    if(userType.equals("student"))
		    	index = 6;
		    category = statsCategory(userType, convertValue(strArray[index]), statsMap);	

		    stat = doc.createElement(parentName);			
			tmpMap = new LinkedHashMap();
	        tmpMap.put("segment_type", "External");
			XmlUtility.buildAttribute(doc, stat, tmpMap);
	         
			tmpMap = new LinkedHashMap();
	        tmpMap.put("statistic_category", category);
	        tmpMap.put("category_type", "Affiliation");
	       
	        Vector tmpVec = buildElements(doc, tmpMap);
	        Element tmp = null;
	        for(int i = 0; i < tmpVec.size(); i++) {
	            tmp = (Element)tmpVec.get(i);
	            stat.appendChild(tmp);
	        }

	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return stat;		
	}
	
	public static String statsCategory(String userType, String data, Map statsMap) {
	    String category = "", key = "", value = "";
	    String[] strArray = null;
	    if(!data.equals("")) {
	    	data = data.replaceFirst ("^0*", "");
	    	if(statsMap != null) {
	        	for (Iterator i = statsMap.keySet().iterator(); i.hasNext();) {
				    key = (String) i.next();
					value = statsMap.get(key).toString();
					strArray = value.split(",");
					for(int j = 0; j < strArray.length; j++) {
						if(strArray[j].equals(data)) {
						    category = key;
						    break;
						}
					}
				}
            }
	    	
	    }
		return category;
	}
	
	public static Map loadStatsMap(String fileName) {
	    Map statsMap = null;
	    Properties statsCategory = null;
		try {
			statsMap = new HashMap();
			statsCategory = FileUtils.loadProperties(fileName);

			String key = "", value = "";
			for (Enumeration e = statsCategory.propertyNames(); e.hasMoreElements();) {
				key = (String) e.nextElement();
				if(key != null) {
					value = statsCategory.getProperty(key);
					statsMap.put(key, value);
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}	    		
	    return statsMap;
	}

	public static Map loadUserGroupMap(String fileToRead) {
	    Map userGroupMap = new HashMap();
	    BufferedReader inFile = null;
		try {
			String lineIn = "", tmpUserGroup = "";
			String[] strArray = null;
			inFile = new BufferedReader(new FileReader(fileToRead));
			while (((lineIn = inFile.readLine()) != null)
					&& !(lineIn.trim().equals(""))) {
			    lineIn = lineIn.trim();	
			    strArray = lineIn.split("\t");
				tmpUserGroup = strArray[5];
				if(userGroupMap.containsKey(strArray[0])) {
					 if(tmpUserGroup.equals(GRAD_GROUP)) {
						 userGroupMap.put(strArray[0], tmpUserGroup);
					 } 
				} else {
					userGroupMap.put(strArray[0], tmpUserGroup);
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}	    		
	    return userGroupMap;
	}

	public static boolean downloadUser(String data, String userType, String expireDate) throws Exception {
		boolean result = false;
		boolean isAfterTmp = false;
		Date recordEndDateTmp = null;

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = new Date();
		recordEndDateTmp = format.parse(expireDate);
		String recordEndDateStr = format.format(recordEndDateTmp), currentDateStr = format.format(currentDate);

		isAfterTmp = recordEndDateTmp.after(currentDate);

		if(includeBlock(data,"email", null, userType) && (isAfterTmp || recordEndDateStr.equals(currentDateStr)) ) {
			result = true;
		}
		return result;
	}

	public static String systemId(String data, String userType) {
		String[] strArray = data.split("\t");
		String value = "";
		if(userType.equals(EMPLOYEE)) {			
			value = convertValue(strArray[14]);			
		} else {
		    value = convertValue(strArray[13]);	
		}

		return value;
	}
		
	public static boolean includeBlock(String data, String type, Map statsMap, String userType) {
		String[] strArray = data.split("\t");
		String value = "";
		boolean result = true;
		int index = 0; 

		if(userType.equals(EMPLOYEE)) {
			if (type.equals("phone")) {
				index = EMPLOYEE_PHONE_INDEX;
			} else if (type.equals("email")) {
				index = EMPLOYEE_EMAIL_INDEX;
			} else if (type.equals("systemId")) {
				index = EMPLOYEE_SYS_ID_INDEX;
			} else if (type.equals("UNIV_ID")) {
				index = EMPLOYEE_UNIV_ID_INDEX;
			} else if (type.equals("BARCODE")) {
				index = EMPLOYEE_BARCODE_INDEX;
			} else if (type.equals("mailcode")) {
				index = EMPLOYEE_MAILCODE_INDEX;
			}
			value = convertValue(strArray[index]);
			if (type.equals("employeeStats")) {
				value = statsCategory(EMPLOYEE, convertValue(strArray[18]), statsMap);
			}
		} else {			
			if (type.equals("phone")) {
				index = STUDENT_PHONE_INDEX;
			} else if (type.equals("email")) {
				index = STUDENT_EMAIL_INDEX;
			} else if (type.equals("systemId")) {
				index = STUDENT_SYS_ID_INDEX;
			} else if (type.equals("UNIV_ID")) {
				index = STUDENT_UNIV_ID_INDEX;
			} else if (type.equals("BARCODE")) {
				index = STUDENT_BARCODE_INDEX;
			} else if (type.equals("schoolAddress")) {
				index = SCHOOL_ADDRESS_INDEX;
			} else if (type.equals("homeAddress")) {
				index = HOME_ADDRESS_INDEX;				
			}
			value = convertValue(strArray[index]);
		}
		if(value.equals("") || (type.equals("email") && !value.endsWith("@ucsd.edu")))
	       result = false;

		return result;
	}
	
	public static String convertDate(String inputDate) {
		String dateStr = inputDate;
		try {            
            DateFormat srcDf = new SimpleDateFormat("MM-dd-yy");
             
            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);
             
            DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");
              
            // format the date into another format
            dateStr = destDf.format(date);             
		} catch (ParseException e) {
		    e.printStackTrace();
		} 
		return dateStr;
	}
	
	public static String parseAddress(String data, String type) {
		String address = data.replace("none","").trim();
		boolean isUsAddress = false;
		if(address.matches(".*\\d{3}"))
			isUsAddress = true;
		if(isUsAddress) {
			if(type.equals("line")) {
				address = address.substring(0, address.lastIndexOf("$"));
			} else if (type.equals("city")) {
				address = address.substring(address.lastIndexOf("$"), address.lastIndexOf(","));			
			} else if (type.equals("state")) {
				address = address.substring(address.lastIndexOf(",")+1, address.lastIndexOf(",")+4);			
			} else if (type.equals("zipcode")) {
				address = address.substring(address.lastIndexOf(",")+4);			
			}
		} else if(!type.equals("line")){
			address = ""; 
		}
		return address.trim();
	}
	
	public static void mergeFiles(String input1, String input2, String fileToWrite) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		    dbf.setNamespaceAware(true);
		    DocumentBuilder db = dbf.newDocumentBuilder();
	
		    File file1 = new File(input1);
		    Document doc1 = db.parse(file1);
		    Element rootElement1 = doc1.getDocumentElement();
	
		    File file2 = new File(input2);
		    Document doc2 = db.parse(file2);
		    Element rootElement2 = doc2.getDocumentElement();
		    // Copy Attributes
		    NamedNodeMap namedNodeMap2 = rootElement2.getAttributes();
		    for (int x = 0; x < namedNodeMap2.getLength(); x++) {
		      Attr importedNode = (Attr) doc1.importNode(namedNodeMap2.item(x), true);
		      rootElement1.setAttributeNodeNS(importedNode);
		    }
	
		    // Copy Child Nodes
		    NodeList childNodes2 = rootElement2.getChildNodes();
		    for (int x = 0; x < childNodes2.getLength(); x++) {
		      Node importedNode = doc1.importNode(childNodes2.item(x), true);
		      rootElement1.appendChild(importedNode);
		    }
	
		    // Output Document
		    writeXmlToFile(doc1, fileToWrite);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void writeNoSysIdPatron(Vector patronList, String sysIdToken) {
		String marcFilesDir = null;
		PrintWriter printWriter = null;

	    try {
	        InitialContext jndi = new InitialContext();
	        marcFilesDir = (String)jndi.lookup("java:comp/env/marcFilePath");
	    } catch (NamingException e) {
	        e.printStackTrace();
	    }
        try {
            printWriter = new PrintWriter(new BufferedOutputStream(new FileOutputStream(marcFilesDir + "noSystemIdPatrons.txt")));
			for (int j = 0; j < patronList.size(); j++) {
				writeSystemId(sysIdToken, printWriter, patronList.elementAt(j).toString());
			}            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                printWriter.close();
            } catch (Exception e) {}
        }
	}
	
	public static void writeSystemId(String sysIdToken, PrintWriter pw, String patronId) {	 
        try {
            String tmpSysId = null, reqType = "pid";
            if(!patronId.startsWith("a")) {
        	    reqType = "ucpath_emplid"; 
            }
    	    tmpSysId = Utility.systemID(patronId, reqType, sysIdToken);
    		pw.print(patronId+"-"+tmpSysId+"\n");
	    	Utility.delay(2, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public static void generateNewSystemId(String sysIdToken) {
		String marcFilesDir = null;
        BufferedReader in = null;
	    try {
	        InitialContext jndi = new InitialContext();
	        marcFilesDir = (String)jndi.lookup("java:comp/env/marcFilePath");
	    } catch (NamingException e) {
	        e.printStackTrace();
	    }
        try {
            in = new BufferedReader(new FileReader(marcFilesDir + "noSystemIdPatrons.txt"));
            String lineIn = null, tmpSysId = null, reqType = "pid";
            int count = 1;
            String[] strArray = null;
            while(((lineIn = in.readLine()) != null) && !(lineIn.trim().equals(""))) {                    
                if(!lineIn.startsWith("a"))
        	    	reqType = "ucpath_emplid";
    	        strArray = lineIn.trim().split("-");	            	    	
                if(strArray.length == 1 || strArray[1].equals("null") || strArray[1].isEmpty()) {                
    	    	    tmpSysId = Utility.systemID(strArray[0], reqType, sysIdToken);
                    System.out.println(count+"-getting sysId for "+lineIn +"---"+tmpSysId);

    	            if (tmpSysId == null || tmpSysId.equals("null") || tmpSysId.isEmpty()) {
   	    	            Utility.postSystemID(strArray[0], reqType, sysIdToken);   
   	                    System.out.println(count+"-creating new sysId for "+lineIn);
    	            }
	    	        Utility.delay(3, TimeUnit.SECONDS);
            	    count++;
                }           
            }    
            System.out.println("Done creating systemId for "+count+" patrons");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (Exception e) {}
        }
	}
}
