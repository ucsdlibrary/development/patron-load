package edu.ucsd.library.patronload.apps;

/*
 * doSystemId.java
 *
 */


import java.io.File;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class doSystemId {

	/** Creates new dofull */
	public doSystemId() {
	}
	
	/**
	* @param args the command line arguments
	* args[0] = destination directory
	* args[1] = properties directory
	*/
	public static void main(String args[]) {

		if ((args == null) || (args.length < 2)) {
			System.out.println(
				"\nSyntax: java doSystemId [destination directory] [properties dir]");
		} else {
			
			String prop_affiliations = args[1] + File.separator + "emp_affiliations.properties";
			String prop_employee_type = args[1] + File.separator + "employee_types.properties";
			String prop_original = args[1] + File.separator + "patron_load.properties";
			try {
				fullquery_employee.grabData(prop_affiliations, prop_employee_type, prop_original, args[0] + File.separator + "full_raw_active_employee.txt", false, null);
			    boolean createSystemId = true;
				fullquery.grabData(args[1], args[0] + "full_raw_file.txt", null);
				mergeaddr.mergeAddresses(
					args[0] + "full_raw_file.txt",
					args[0] + "full_merged_file.txt");
				makexml_student.makeXml(
					args[1],
					args[0] + "full_merged_file.txt",
					args[0] + Upload.filePrefix()+"-temp.xml",
					"full_raw_active_employee.txt", createSystemId);
				File tmp = new File(args[0] + Upload.filePrefix()+"-temp.xml");
				if (tmp.exists())
					tmp.delete();
				System.out.println("Done generating systemId file");
			} catch (Exception e) {
			}					
		}
	}
}
