package edu.ucsd.library.patronload.apps;

import java.io.File;
import java.text.Format;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class select_employee_xml {
	
	public select_employee_xml() {
	}
	
	/**
	* @param args the command line arguments
	* args[0] = destination directory
	* args[1] = properties directory
	*/
	public static void main(String args[]) {

		if ((args == null) || (args.length < 3)) {
			System.out.println(
				"\nSyntax: java select_employee_xml [destination directory] [properties dir] [idList]");
		} else {
			
			String prop_affiliations = args[1] + File.separator + "emp_affiliations.properties";
			String prop_employee_type = args[1] + File.separator + "employee_types.properties";
			String prop_original = args[1] + File.separator + "patron_load.properties";
			try {
				fullquery_employee.grabData(prop_affiliations, prop_employee_type, prop_original, 
						                    args[0] + File.separator + "select_raw_active_employee.txt", false,
						                    "," + args[2]);
			
				fullquery.grabData(args[1], args[0] + "select_raw_file.txt", "," + args[2]);
				mergeaddr.mergeAddresses(
					args[0] + "select_raw_file.txt",
					args[0] + "select_merged_file.txt");
				makexml_student.makeXml(
					args[1],
					args[0] + "select_merged_file.txt",
					args[0] + Upload.filePrefix().replace("full","individuals")+"-temp.xml",
					"select_raw_active_employee.txt", false);	
				File tmp = new File(args[0] + Upload.filePrefix().replace("full","individuals")+"-temp.xml");
				if (tmp.exists())
					tmp.delete();
				System.out.println("Done generating file");

			} catch (Exception e) {
			}					
		}
	}
}
