package edu.ucsd.library.patronload.apps;

/*
 * fullquery.java
 *
 * This is the Java port of "darwin.query", the perl version
 *
 * Created on June 18, 2002, 12:32 PM
 */

/**
 *
 * @author Joseph Jesena
 */

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Properties;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import edu.ucsd.library.util.FileUtils;

import javax.naming.NamingException;

public class fullquery {

    /** Creates new fullquery */
    public fullquery() {
    }

    /**
     * @param args
     *            the command line arguments args[0] = pathToProperties args[1] =
     *            fileToWrite
     */
    public static void main(String args[]) {

        if ((args == null) || (args.length < 3)) {
            System.out
                    .println("\nSyntax: java fullquery [pathToProperties] [fileToWrite] [idList]");
        } else {
            grabData(args[0], args[1], args[2]);
        }
    }

    /**
     * Write data out to the file
     *
     * @param pathToProperties
     *            Path to the properties file
     * @param fileToWrite
     *            Path to the file to write results to
     */
    public static void grabData(String pathToProperties, String fileToWrite, String idList) {

        PrintWriter pw = null;

        //--support a collection of 300,000 students, max
        student_ids = new HashMap(300000);

        //--- Create the file stream here to output to file
        try {        	
			String sysIdToken = Utility.getToken(pathToProperties, "system_id_token.txt", "getSystemIdToken.sh");
			String systemIdQuery = "select a.pid, s.id from affiliates_dw.system s left join affiliates_dw.affiliates_safe_attributes a "
					+ "on s.aid=a.aid where s.system_id=41 and a.pid is not null";
			sysIdMap = Utility.loadSystemId(systemIdQuery);
			String emailQuery = "select a.pid, a.official_email from affiliates_dw.affiliates_safe_attributes a "
					+ "where a.pid is not null and a.official_email is not null and a.official_email like '%@ucsd.edu'";
			studentEmailMap = Utility.loadSystemId(emailQuery);
			
            pw = new PrintWriter(new BufferedOutputStream(new FileOutputStream(
                    fileToWrite)));

            getRawData(pathToProperties, pw, idList);
            getGradStudentData(pathToProperties, pw, idList);
        } catch (IOException ioe) {
            System.out.println(ioe);
        } finally {
            try {
                if (pw != null)
                    pw.close();
            } catch (Exception e1) {
            }
        }
    }

    /**
     * Method to retreive data from database and output to raw file.
     *
     * @param pathToProperties
     *            Path to the properties file
     * @param pw
     *            Path to the file to write results to
     */
    public static void getRawData(String pathToProperties, PrintWriter pw, String idList) {

        if (!pathToProperties.equals("")) {
            pathToProperties += File.separator;
        }

        // load the properties file to get the current quarter code
        Properties myProp = null;
        try {
            myProp = FileUtils.loadProperties(pathToProperties
                    + "patron_load.properties");
        } catch (IOException ioe) {
            System.out.println("Error loading properties file in fullquery.getRawData!");
            return;
        }

        String trm_term_code = "", sysId = "";
        String term = (String) myProp.get("quartercode");
        term = term.trim();

        trm_term_code = "T.trm_term_code = '" + term + "' and ";
        String year = term.substring(2, term.length());

        if (term.toUpperCase().startsWith("SU")) {
            trm_term_code = "(";
            trm_term_code += "(T.trm_term_code = 'S1" + year + "') or ";
            trm_term_code += "(T.trm_term_code = 'S2" + year + "') or ";
            trm_term_code += "(T.trm_term_code = 'S3" + year + "') or ";
            trm_term_code += "(T.trm_term_code = 'SU" + year + "')";
            trm_term_code += ") and ";
        }

        Statement stmt = null;
        ResultSet rs = null;
        Connection db2Conn = null;

        try {
            db2Conn = Utility.getDb2Connection();
			String sysIdToken = Utility.getToken(pathToProperties, "system_id_token.txt", "getSystemIdToken.sh");
			System.out.println("sysIdMap size:"+sysIdMap.size()+"-studentEmail size:"+studentEmailMap.size());
            stmt = db2Conn.createStatement();

            String query = 	"select S.stu_pid, S.stu_name, '' as ssn, " +
            				"T.stt_registration_status_code, T.trm_term_code as last_enrolled, " +
            				"substr(T.stt_academic_level,1,1) as academic_level, T.maj_major_code, " +
            				"A.adr_address_type, rtrim(substr(char(year(A.adr_start_date)),3,4)) " +
            				"concat rtrim(ltrim(char(month(A.adr_start_date)))) concat " +
            				"rtrim(ltrim(char(day(A.adr_start_date)))) as startdate, " +
            				"rtrim(substr(char(year(A.adr_end_date)),3,4)) concat " +
            				"rtrim(ltrim(char(month(A.adr_end_date)))) concat " +
            				"rtrim(ltrim(char(day(A.adr_end_date)))) as stopdate, " +
            				"A.adr_address_line_1, A.adr_address_line_2, A.adr_address_line_3, " +
            				"A.adr_address_line_4, A.adr_city, substr(A.adr_phone,1,3) " +
            				"as area_code, substr(A.adr_phone,5,3) as exchange, " +
            				"substr(A.adr_phone,9,4) as sqid, char(' ',4) as extension, " +
            				"A.adr_state, A.adr_zip, A.co_country_code, E.em_address_line, " +
            				"I.student_barcode, E.em_address_type, SI.id from student_db.s_student S " +
            				"inner join student_db.s_student_term T on S.stu_pid = T.stu_pid and " +
            				trm_term_code + " T.stt_major_primary_flag = 'Y' and S.deceased_flag != 'Y' and " +
            				"stt_registration_status_code in ('EN', 'RG') and T.stt_academic_level in ('UN') " +
            				"inner join student_db.s_address A on T.stu_pid = A.stu_pid and " +
            				"(adr_address_type = 'CM' or adr_address_type = 'PM') left outer join student_db.s_email E " +
            				"ON S.stu_pid = E.stu_pid and (E.em_address_type = 'EMC' or E.em_address_type = 'EMH' or E.em_address_type is null) " +
            				"and (E.em_end_date is null or E.em_end_date !< current date) LEFT OUTER JOIN " +
            				"affiliates_dw.rosetta_stone_barcode_v I ON S.stu_pid = I.stu_pid " +
            				"LEFT JOIN affiliates_dw.affiliates_safe_attributes SA ON S.stu_pid = SA.pid " +
            				"LEFT JOIN affiliates_dw.system SI ON SA.aid = SI.aid and SI.system_id = 41 order by S.stu_pid, A.adr_start_date, A.adr_end_date";

            try {
                if(marcFilesDir == null)
                	setMarcFilesDir(pathToProperties);
                FileUtils.confirmDir(marcFilesDir);
                FileUtils.stringToFile(query, marcFilesDir
                        + "full_query_undergrad_student.sql");
            } catch (IOException ioe) {
            	ioe.printStackTrace();
            }

            rs = stmt.executeQuery(query);

            ResultSetMetaData rsms = rs.getMetaData();
            int numcol = rsms.getColumnCount();
            String tmpStudentId = null, tmpStr = null, tmp_sys_id = null, tmpEmail = null;

            while (rs.next()) {
            	tmpStudentId = rs.getString(1).trim();
            	tmpEmail = "?";
            	if(rs.getString(23) == null) {
            		if(studentEmailMap.containsKey(tmpStudentId))
            			tmpEmail = studentEmailMap.get(tmpStudentId).toString();
            		else
            		    System.out.println("no email for student id:"+tmpStudentId);
            	} else {
            	    tmpEmail = rs.getString(23).trim();
            	}
            	
	            if(sysIdMap.containsKey(tmpStudentId)) {
	                tmp_sys_id = (String)sysIdMap.get(tmpStudentId);
	            }            	

                if(idList == null || idList.contains(","+tmpEmail) || idList.contains(tmpStudentId.toLowerCase()) || idList.contains(tmpStudentId.toUpperCase()) || idList.contains(tmp_sys_id.trim())) {
                	for (int i = 1; i <= numcol; i++) {
                        tmpStr = rs.getString(i);

                        if ((tmpStr == null) || (tmpStr.trim().toLowerCase().equals(""))) {
                            // Put in question mark if its null or blank
                            tmpStr = "?";
                        }

                        if (i == 1) {
                            //--get a list of all the student IDs
                            student_ids.put(tmpStr.toLowerCase().trim(), "");
                            tmpStudentId = tmpStr.toUpperCase().trim();
                        }
                     
                        pw.print(tmpStr);
                        pw.print("\t");
                    }
                    if(sysIdMap.containsKey(tmpStudentId)) {
                	    pw.print(sysIdMap.get(tmpStudentId)+"\t");
                    } else {
                    	sysId = "";
				        pw.print(sysId+"\t");
                	    //pw.print("?\t");
                    }
                    pw.print("\n");
                } 
            }
        } catch (SQLException | NamingException ex) {
            System.err.println("getRawData Exception: " + ex);
            //ex.printStackTrace(System.out);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                if (db2Conn != null)
					db2Conn.close();
            } catch (SQLException e) {
            }
        }
    }

    /**
     * Method to retreive Grad Students data from database and output to raw file.
     *
     * @param pathToProperties
     *            Path to the properties file
     * @param pw
     *            Path to the file to write results to
     */
    public static void getGradStudentData(String pathToProperties, PrintWriter pw, String idList) {

        if (!pathToProperties.equals("")) {
            pathToProperties += File.separator;
        }

        // load the properties file to get the current quarter code
        Properties myProp = null;
        try {
            myProp = FileUtils.loadProperties(pathToProperties
                    + "patron_load.properties");
        } catch (IOException ioe) {
            System.out.println("Error loading properties file in fullquery.getGradStudentData!");
            return;
        }

        String trm_term_code = "";
        String term = (String) myProp.get("quartercode");
        term = term.trim();


        trm_term_code = "T.trm_term_code = '" + term + "' and ";
        String year = term.substring(2, term.length());

        String db2Driver = (String) myProp.get("db2driver");
		String db2Username = (String) myProp.get("db2username");
		String db2Password = (String) myProp.get("db2password");
		String db2Connection = (String) myProp.get("db2connection");

		if (term.toUpperCase().startsWith("SU")) {
			trm_term_code = "(";
            trm_term_code += "(T.trm_term_code = 'S1" + year + "') or ";
            trm_term_code += "(T.trm_term_code = 'S2" + year + "') or ";
            trm_term_code += "(T.trm_term_code = 'S3" + year + "') or ";
            trm_term_code += "(T.trm_term_code = 'FA" + year + "') or ";
            trm_term_code += "(T.trm_term_code = 'SU" + year + "')";
            trm_term_code += ") and ";
        }

        try {
        	Class.forName(db2Driver).newInstance();
        } catch (Exception E) {
            System.err.println("Error: Unable to load driver: " + db2Driver);
            E.printStackTrace();
            System.exit(1);
        }

        Statement stmt = null;
        ResultSet rs = null;
        Connection db2Conn = null;

        try {

			db2Conn = DriverManager.getConnection(
					db2Connection,
					db2Username,
					db2Password);

            stmt = db2Conn.createStatement();

            String query = "select S.stu_pid, S.stu_name, '' as ssn, " +
            		"T.stt_registration_status_code, T.trm_term_code as last_enrolled, " +
            		"substr(T.stt_academic_level,1,1) as academic_level, T.maj_major_code, " +
            		"A.adr_address_type, rtrim(substr(char(year(A.adr_start_date)),3,4)) " +
            		"concat rtrim(ltrim(char(month(A.adr_start_date)))) concat " +
            		"rtrim(ltrim(char(day(A.adr_start_date)))) as startdate, " +
            		"rtrim(substr(char(year(A.adr_end_date)),3,4)) concat " +
            		"rtrim(ltrim(char(month(A.adr_end_date)))) concat " +
            		"rtrim(ltrim(char(day(A.adr_end_date)))) as stopdate, " +
            		"A.adr_address_line_1, A.adr_address_line_2, A.adr_address_line_3, " +
            		"A.adr_address_line_4, A.adr_city, substr(A.adr_phone,1,3) " +
            		"as area_code, substr(A.adr_phone,5,3) as exchange, " +
            		"substr(A.adr_phone,9,4) as sqid, char(' ',4) as extension, " +
            		"A.adr_state, A.adr_zip, A.co_country_code, E.em_address_line, " +
            		"I.student_barcode, E.em_address_type, SI.id from student_db.s_student S " +
            		"inner join student_db.s_student_term T on S.stu_pid = T.stu_pid and S.deceased_flag != 'Y' and " +
            		trm_term_code+" T.stt_major_primary_flag = 'Y' and stt_registration_status_code in ('EN', 'RG') and " +
            		"T.stt_academic_level in ('GR','MD','PH') inner join student_db.s_address A on " +
            		"T.stu_pid = A.stu_pid and (adr_address_type = 'CM' or adr_address_type = 'PM') " +
            		"left outer join student_db.s_email E ON S.stu_pid = E.stu_pid and (E.em_address_type = 'EMC' or E.em_address_type = 'EMH' " +
            		"or E.em_address_type is null) and (E.em_end_date is null or " +
            		"E.em_end_date !< current date) LEFT OUTER JOIN affiliates_dw.rosetta_stone_barcode_v I " +
            		"ON S.stu_pid = I.stu_pid LEFT JOIN affiliates_dw.affiliates_safe_attributes SA " +
            		"ON S.stu_pid = SA.pid LEFT JOIN affiliates_dw.system SI ON SA.aid = SI.aid and SI.system_id = 41 " +
            		"order by S.stu_pid, A.adr_start_date, A.adr_end_date";

            try {

                FileUtils.confirmDir(marcFilesDir);
                FileUtils.stringToFile(query, marcFilesDir
                        + "full_query_all_grad_students.sql");
            } catch (IOException ioe) {
            }

            rs = stmt.executeQuery(query);

            ResultSetMetaData rsms = rs.getMetaData();
            int numcol = rsms.getColumnCount();
            String tmpStudentId = null, tmp_sys_id = null, tmpEmail;

            while (rs.next()) {
            	tmpStudentId = rs.getString(1).trim();
            	tmpEmail = rs.getString(23).trim();
	            if(sysIdMap.containsKey(tmpStudentId)) {
	                tmp_sys_id = (String)sysIdMap.get(tmpStudentId);
	            } 
                if(idList == null || idList.contains(","+tmpEmail) || idList.contains(tmpStudentId.toLowerCase()) || idList.contains(tmpStudentId.toUpperCase()) || idList.contains(tmp_sys_id.trim())) {
	                for (int i = 1; i <= numcol; i++) {
                        String tmpStr = rs.getString(i);

                        if ((tmpStr == null) || (tmpStr.trim().toLowerCase().equals(""))) {
                            // Put in question mark if its null or blank
                            tmpStr = "?";
                        }

                        if (i == 1) {
                            //--get a list of all the student IDs
                            student_ids.put(tmpStr.toLowerCase().trim(), "");
                        }

                        pw.print(tmpStr);
                        pw.print("\t");
                    }
                    pw.print("\n");
                }
            }

        } catch (SQLException ex) {
            System.err.println("Exception: " + ex);
            //ex.printStackTrace(System.out);
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
                /*if (conn != null)
                    conn.close();*/
                if (db2Conn != null)
					db2Conn.close();
            } catch (SQLException e) {
            }
        }
    }


    public static void setMarcFilesDir(String mDir) {
    	marcFilesDir = mDir;
    }

    private static HashMap student_ids;
    private static Map sysIdMap;
    public static Map studentEmailMap;
    private static String marcFilesDir;
}

/*******************************************************************************
 * Test query:
 *
 * select
 *
 * S.stu_pid, S.stu_name, ssn=convert(char(9),' '),
 * T.stt_registration_status_code, last_enrolled = 'SP04', academic_level =
 * substring(T.stt_academic_level,1,1), T.maj_major_code, A.adr_address_type,
 * startdate=convert(char(12),A.adr_start_date,12), stopdate=convert(char(6),
 * A.adr_end_date,12), A.adr_address_line_1, A.adr_address_line_2,
 * A.adr_address_line_3, A.adr_address_line_4, A.adr_city, area_code =
 * substring(A.adr_phone,1,3), exchange = substring(A.adr_phone,5,3), sqid =
 * substring(A.adr_phone,9,4), extension = convert(char(4), ' '), A.adr_state,
 * A.adr_zip, A.co_country_code, E.em_address_line, I.barcode
 *
 * from s_student_term T, s_student S, s_address A, s_email E,
 * proxy_db..idcard_v I
 *
 * where
 *
 * (S.stu_pid = T.stu_pid) and T.trm_term_code = 'SP04' and
 * T.stt_major_primary_flag = 'Y' and T.stu_pid = A.stu_pid and
 * (adr_address_type = 'CM' or adr_address_type = 'PM') and
 * stt_registration_status_code in ('EN', 'RG') and S.stu_pid = E.stu_pid and
 * E.em_address_type = 'EMC' and (S.stu_pid *= pid) and (E.em_end_date = null or
 * E.em_end_date ! < getdate())
 *
 * order by S.stu_pid, A.adr_start_date, A.adr_end_date
 */
