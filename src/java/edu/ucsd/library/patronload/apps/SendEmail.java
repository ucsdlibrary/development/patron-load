package edu.ucsd.library.patronload.apps;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class SendEmail {
    private static String propertiesFilePath;

    public static void execute(Vector data) throws Exception {
        Map<String,String> propertyMap = patronloadProperties();
        Properties props = defaultEmailProperties();

        final String username = propertyMap.get("email_username");
        final String password = propertyMap.get("email_password");
        final String to = propertyMap.get("email_to");
        final String from = propertyMap.get("email_from");
        props.put("mail.smtp.user", username);
        StringBuffer emailContent = new StringBuffer();
        Session session = Session.getInstance(props, new UCSDAuthenticator(username, password));
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject("Patron Load Data Problem");
            emailContent.append("The following record(s) has an issue and has been excluded from the Alma XML file.  Please review and resolve.\n\n");
		    for (int i = 0; i < data.size() ; i++) {
	      	    emailContent.append(data.elementAt(i)+"\n");
	        }             
            message.setText(emailContent.toString());

            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    // Load the properties from patronload.properties to use in sending email in execute()
    private static Map<String,String> patronloadProperties() throws Exception {
        Map<String,String> propertyMap = new HashMap();
        try {
            InitialContext jndi = new InitialContext();
            propertiesFilePath = (String)jndi.lookup("java:comp/env/marcFilePath");           
            FileReader reader = new FileReader(propertiesFilePath + "patronload.properties");
            Properties p = new Properties();
            p.load(reader);

            Enumeration keys = p.propertyNames();
            while(keys.hasMoreElements()) {
                String key = (String)keys.nextElement();
                propertyMap.put(key,p.getProperty(key));
            }
            reader.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return propertyMap;
    }

    // Define default properties for sending email
    public static Properties defaultEmailProperties() {
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.ucsd.edu");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth.mechanisms", "PLAIN");
        props.put("mail.debug", "true");
        return props;
    }
}

class UCSDAuthenticator extends Authenticator {
     String user;
     String pw;
     public UCSDAuthenticator (String username, String password)
     {
        super();
        this.user = username;
        this.pw = password;
     }
    public PasswordAuthentication getPasswordAuthentication()
    {
       return new PasswordAuthentication(user, pw);
    }
}
