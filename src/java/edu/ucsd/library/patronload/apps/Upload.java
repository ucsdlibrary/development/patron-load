package edu.ucsd.library.patronload.apps;

import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import io.minio.errors.*;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Upload {
    public static void send(){
        final String bucketPath = "synchronize/";
        prepareZipFile();
        final String filename = filePrefix() + ".zip";
        try {
            final String bucket = (String)context().lookup("java:comp/env/minio/bucket");
            final String marcFilesDir = (String)context().lookup("java:comp/env/marcFilePath");
            client().uploadObject(
                UploadObjectArgs.builder()
                    .bucket(bucket)
                    .object(bucketPath + filename)
                    .filename(marcFilesDir + filename)
                    .build());
        } catch (MinioException | IOException | InvalidKeyException | NoSuchAlgorithmException | NamingException e) {
            System.out.println("Error occurred: " + e);
        }
    }

    /**
     * Create an instance of a MinioClient
     * @return instance of a MinioClient
     */
    private static MinioClient client(){
        String endpoint = null;
        String accessKey = null;
        String secretKey = null;
        try {
            endpoint = (String)context().lookup("java:comp/env/minio/endpoint");
            accessKey = (String)context().lookup("java:comp/env/minio/accessKey");
            secretKey = (String)context().lookup("java:comp/env/minio/secretKey");
        } catch (NamingException e) {
            System.out.println(e);
        }
        MinioClient minioClient =
            null;
        try {
            minioClient = MinioClient.builder().endpoint(new URL(endpoint))
                .credentials(accessKey, secretKey)
                .build();
        } catch (MalformedURLException e) {
            System.out.println(e);
        }
        return minioClient;
    }
    /**
     * Create a zip file in the marcFilesDir holding the contents of the current Alma XML file
     */
    private static void prepareZipFile() {
        String marcFilesDir = null;
        try {
            marcFilesDir = (String)context().lookup("java:comp/env/marcFilePath");
        } catch (NamingException e) {
            System.out.println(e);
        }
        String xmlFilePath = marcFilesDir + filePrefix() + ".xml";
        String zipPath = marcFilesDir + filePrefix() + ".zip";

        try (ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipPath))) {
            File fileToZip = new File(xmlFilePath);
            zipOut.putNextEntry(new ZipEntry(fileToZip.getName()));
            Files.copy(fileToZip.toPath(), zipOut);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * Creates the file prefix for a given file using patronload file naming convention.
     * Examples:
     *  - full-2021-May-25.xml
     *  - full-2021-May-25.zip
     * @return
     */
    public static String filePrefix(){
        final String filenamePrefix = "full";
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MMMM-dd");
        String strDate = sdf.format(date);
        return filenamePrefix+"-"+strDate;
    }

    /**
     * Create an initialcontext for pulling jndi variables in the class
     * @return
     */
    private static InitialContext context(){
        InitialContext jndi = null;
        try {
            jndi = new InitialContext();
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return jndi;
    }
}
