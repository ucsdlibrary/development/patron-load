package edu.ucsd.library.patronload.apps;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Reader;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import edu.ucsd.library.util.TextUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class makexml_student {

	public static void makeXml(String propsFolder, String fileToRead, String fileToWrite, String empFile, boolean createSystemId) {
		BufferedReader in = null;
		Document doc = null;
		String sysIdToken = null;
        Vector newPatronNoSysIdList = new Vector();		
		sysIdToken = Utility.getToken(propsFolder, "system_id_token.txt", "getSystemIdToken.sh");
        
		try {
			Reader inputReader = new InputStreamReader(new FileInputStream(fileToRead),"UTF-8");
        	        in = new BufferedReader(inputReader);

			String lineIn = "", studentId = "";	
			String[] dataArray = null;
	        Vector elementsVector = null;
	   	    doc = XmlUtility.createDoc();
	   	    Element rootElement = doc.createElement("users");
            doc.appendChild(rootElement);
            boolean include = true, includeSystemID = true;
            Map statsCategoryMap = XmlUtility.loadStatsMap(propsFolder+"statsCategory.properties");
            Map userGroupMap = XmlUtility.loadUserGroupMap(fileToRead);
            Vector systemIdVec = new Vector(), studentIdVec = new Vector();
            int noStudentSysId = 0;
			String token = Utility.getToken(propsFolder, "access_token.txt", "getAccessToken.sh");
			Map prefNameMap = makemarc.loadPreferredName(propsFolder, token);
			
			while (((lineIn = in.readLine()) != null)
				&& !(lineIn.trim().equals(""))) {
				lineIn = lineIn.trim();	
				
				//remove the question marks
				lineIn = TextUtils.strReplace(lineIn, "?", "none");
				//include = XmlUtility.downloadUser(lineIn, XmlUtility.STUDENT);
				include = XmlUtility.includeBlock(lineIn,"email", null, XmlUtility.STUDENT);
				includeSystemID = XmlUtility.includeBlock(lineIn,"systemId", null, XmlUtility.STUDENT);
				studentId = lineIn.substring(0, lineIn.indexOf("\t"));
				if(!includeSystemID) {
					noStudentSysId += 1;
		    	    if(!newPatronNoSysIdList.contains(studentId)) {
		    	    	newPatronNoSysIdList.add(studentId);
		    	    }					
				}
				if (include && includeSystemID && !studentIdVec.contains(studentId)) {
					studentIdVec.add(studentId);
					if(!systemIdVec.contains(XmlUtility.systemId(lineIn,XmlUtility.STUDENT)))
						systemIdVec.add(XmlUtility.systemId(lineIn,XmlUtility.STUDENT));
	                elementsVector = XmlUtility.buildElements(doc, loadElements(lineIn, userGroupMap, prefNameMap));
	                elementsVector.add(contactInfoElement(doc, lineIn));
	                elementsVector.add(userIdElement(doc, lineIn, sysIdToken));
	                elementsVector.add(statisticElement(doc, lineIn, statsCategoryMap));
		  	        XmlUtility.writeData(doc, rootElement, elementsVector);
			    }
			}
			System.out.println("Students with No sysID from db count:"+noStudentSysId);			
			makexml_employee.mergeXml(propsFolder, empFile, doc, rootElement, systemIdVec, newPatronNoSysIdList, studentIdVec);

		} catch (IOException ioe) {
			System.out.println("Error in makeXml:+"+ioe);
			ioe.printStackTrace();
		}

		if(createSystemId) {
		    System.out.println("Patrons with new systemId count:"+ newPatronNoSysIdList.size());
		    XmlUtility.writeNoSysIdPatron(newPatronNoSysIdList, sysIdToken);
		    XmlUtility.generateNewSystemId(sysIdToken);
		} else {
			XmlUtility.writeXmlToFile(doc, fileToWrite);
			XmlUtility.unescapeXml(fileToWrite);			
		}
	}

	private static Map loadElements(String lineIn, Map userGroupMap, Map prefNameMap) {
		Map elementMap = null;
		try {
			String[] strArray = null, nameArray = null, prefNameArray = null;
	        String id = "", fullName = "", primaryId = "", firstName = "", lastName = "", middleName = "";
			String userGroup = "", expireDate = "", tmpUserGroupCode = "", tmpEmail = "";
			String prefFirstName = "", prefLastName = "", prefMiddleName = "";
            elementMap = new LinkedHashMap();
			strArray = lineIn.split("\t");
			
			tmpEmail = strArray[11];
			if(!tmpEmail.endsWith("ucsd.edu") && fullquery.studentEmailMap.containsKey(strArray[0])) {
				tmpEmail = fullquery.studentEmailMap.get(strArray[0]).toString();
			}			
			primaryId = XmlUtility.convertValue(tmpEmail);
			
			fullName = XmlUtility.convertValue(strArray[1]);
			expireDate = XmlUtility.expireDateStudent(strArray[5]);
			
			tmpUserGroupCode = strArray[5];
			if(userGroupMap.containsKey(strArray[0])) {
				tmpUserGroupCode = userGroupMap.get(strArray[0]).toString();
			}
			userGroup = XmlUtility.userGroupCode(tmpUserGroupCode);
			
			nameArray = fullName.split(",");
			lastName = nameArray[0];
			if(nameArray.length > 1)
			  firstName = nameArray[1].trim();

			if(prefNameMap.containsKey(strArray[0])) {
				prefNameArray = prefNameMap.get(strArray[0]).toString().split(",");
				prefLastName = prefNameArray[0];
				prefFirstName = prefNameArray[1];
				if(prefNameArray.length > 2) {
					prefMiddleName = prefNameArray[2];
				}
				if(prefFirstName.length() > 0) {
					firstName = prefFirstName.trim();
				}
				if(prefMiddleName.length() > 0) {
					middleName = prefMiddleName.trim();
				}
			}
            String newFullName = "";
            if(!prefLastName.isEmpty())
            	lastName = prefLastName;
                        
            if(firstName.isEmpty() && middleName.isEmpty())
            	newFullName = lastName;
            else
                newFullName = lastName + ", " + firstName + " " + middleName;

			elementMap.put("record_type", "public");
			elementMap.put("primary_id", primaryId);			
			elementMap.put("first_name", firstName);
			elementMap.put("middle_name", middleName);
			elementMap.put("last_name", lastName);
            elementMap.put("full_name", newFullName.trim());
			elementMap.put("pref_first_name", "");
			elementMap.put("pref_middle_name", "");
			elementMap.put("pref_last_name", "");
			elementMap.put("pref_name_suffix", "");
            elementMap.put("user_group", userGroup);
            elementMap.put("preferred_language", "en");
            elementMap.put("expiry_date", expireDate);
            elementMap.put("purge_date", expireDate);
            elementMap.put("account_type", "EXTERNAL");
            elementMap.put("status", "ACTIVE");          
		} catch (Exception e) {
			e.printStackTrace();
		}
   	    return elementMap;   	    
	}

	private static Element contactInfoElement(Document doc, String lineIn) {
		Element contactInfo = null;
		Map tmpMap = null;
		try {			
			 contactInfo  = doc.createElement("contact_info");
			 Element addresses = doc.createElement("addresses");
			 Element address = null;
			 boolean includeAddress = false;
			 if(XmlUtility.includeBlock(lineIn, "schoolAddress", null, XmlUtility.STUDENT)) {
			     address = XmlUtility.addressBlock(doc,"address",lineIn, true, "school");			 
	             addresses.appendChild(address);
	             includeAddress = true;
			 }
			 if(XmlUtility.includeBlock(lineIn, "homeAddress", null, XmlUtility.STUDENT)) {			 
			     address = XmlUtility.addressBlock(doc,"address",lineIn, false, "home");			 
	             addresses.appendChild(address);
	             includeAddress = true;
			 }
			 if(includeAddress)
	             contactInfo.appendChild(addresses);	     
	         if (XmlUtility.includeBlock(lineIn, "email", null, XmlUtility.STUDENT)) {
	             Element emails = doc.createElement("emails");
	             Element email = XmlUtility.emailBlock(doc,"email",lineIn, true, "school");
	             emails.appendChild(email);
	             contactInfo.appendChild(emails);
	         }
	         if (XmlUtility.includeBlock(lineIn, "phone", null, XmlUtility.STUDENT)) {
	             Element phones = doc.createElement("phones");
	             Element phone = XmlUtility.phoneBlock(doc,"phone",lineIn, true, "home");
	             phones.appendChild(phone);
		         contactInfo.appendChild(phones);
	         }
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return contactInfo;  	    
	}
	
	private static Element userIdElement(Document doc, String lineIn, String sysIdToken) {
		Element userIds = null;
		Map tmpMap = null;
		try {			
			 userIds  = doc.createElement("user_identifiers");
			 Element userIdElement = null;
			 if(XmlUtility.includeBlock(lineIn, "BARCODE", null, XmlUtility.STUDENT)) {
				 userIdElement = XmlUtility.userIdBlock(doc,"user_identifier",lineIn,"BARCODE",XmlUtility.STUDENT, sysIdToken);			 
		         userIds.appendChild(userIdElement);
		     }
	         userIdElement = XmlUtility.userIdBlock(doc,"user_identifier",lineIn,"UNIV_ID",XmlUtility.STUDENT, sysIdToken);			 
	         userIds.appendChild(userIdElement);
	         userIdElement = XmlUtility.userIdBlock(doc,"user_identifier",lineIn,"OTHER_ID_4",XmlUtility.STUDENT, sysIdToken);			 
	         userIds.appendChild(userIdElement);
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return userIds;  	    
	}
		
	private static Element statisticElement(Document doc, String lineIn, Map statsMap) {
		Element userIds = null;
		Map tmpMap = null;
		try {			
			 userIds  = doc.createElement("user_statistics");
			 Element userIdElement = XmlUtility.statsBlock(doc,"user_statistic",lineIn, statsMap,XmlUtility.STUDENT);			 
	         userIds.appendChild(userIdElement);
	        
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return userIds;  	    
	}
	
	/**
	* @param args the command line arguments
	* args[0] = folder of patronload properties file
	* args[1] = fileToRead
	* args[2] = fileToWrite
	*/
	public static void main(String args[]) {

		if ((args == null) || (args.length < 3)) {
			System.out.println(
				"\nSyntax: java makexml_student [properties folder] [fileToRead] [fileToWrite] [employeeFile]");
		} else {
			makeXml(args[0], args[1], args[2], args[3], false);
		}
	}

}
