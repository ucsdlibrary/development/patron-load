package edu.ucsd.library.patronload.apps;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Reader;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.lang3.StringEscapeUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class makexml_employee {
	public static void mergeXml(String propsFolder, String fileToRead, Document doc, Element rootElement, Vector systemIdVec, Vector newPatronNoSysIdList, Vector studentIdVec) {
		BufferedReader in = null;
		try {
			
			Reader inputReader = new InputStreamReader(new FileInputStream(propsFolder + fileToRead),"UTF-8");
        	        in = new BufferedReader(inputReader);
        	
			String lineIn = "";	
			String[] dataArray = null;

            boolean include = true, includeSystemID = true;
            Map statsCategoryMap = XmlUtility.loadStatsMap(propsFolder+"statsCategory.properties");
            Vector elementsVector = null;
            int noStaffSysId = 0;
			String sysIdToken = Utility.getToken(propsFolder, "system_id_token.txt", "getSystemIdToken.sh");
			String pid = "";
			while (((lineIn = in.readLine()) != null)
				&& !(lineIn.trim().equals(""))) {
				lineIn = lineIn.trim();	
				dataArray = lineIn.split("\t");
				pid = XmlUtility.pidValue(lineIn);
				include = XmlUtility.downloadUser(lineIn, XmlUtility.EMPLOYEE, XmlUtility.expireDate(dataArray[4], dataArray[19]));
				includeSystemID = XmlUtility.includeBlock(lineIn,"systemId", null, XmlUtility.EMPLOYEE);
                if(!includeSystemID) {
					noStaffSysId += 1;
		    	    if(!newPatronNoSysIdList.contains(dataArray[4].trim())) {
		    	    	newPatronNoSysIdList.add(dataArray[XmlUtility.EMPLOYEE_ID_2_INDEX].trim());
		    	    }                	
                }
				if (!studentIdVec.contains(pid) && include && includeSystemID && !dataArray[4].trim().equals("12") && !systemIdVec.contains(XmlUtility.systemId(lineIn,XmlUtility.EMPLOYEE))) {
					elementsVector = XmlUtility.buildElements(doc, loadElements(lineIn));
	                elementsVector.add(contactInfoElement(doc, lineIn));
	                elementsVector.add(userIdElement(doc, lineIn, sysIdToken));
	                if(XmlUtility.includeBlock(lineIn, "employeeStats", statsCategoryMap, XmlUtility.EMPLOYEE))
	                    elementsVector.add(statisticElement(doc, lineIn, statsCategoryMap));
		  		    XmlUtility.writeData(doc, rootElement, elementsVector);
				}
			}
		} catch (Exception ioe) {
			System.out.println("Error in makeXml:+"+ioe);
			ioe.printStackTrace();
		}
	}
	
	private static Map loadElements(String lineIn) {
		Map elementMap = null;
		try {
			String[] strArray = null;
	        String id = "", fullName = "", primaryId = "", firstName = ".", lastName = "", middleName = "";
			String userGroup = "", expireDate = "";
	        elementMap = new LinkedHashMap();
			strArray = lineIn.split("\t");
			id = XmlUtility.convertValue(strArray[0]);
			fullName = XmlUtility.convertValue(strArray[1]);
			primaryId = XmlUtility.convertValue(strArray[10]);
			lastName = XmlUtility.convertValue(strArray[15]);
			firstName = XmlUtility.convertValue(strArray[16]);
			middleName = XmlUtility.convertValue(strArray[17]);
            userGroup = XmlUtility.userGroupCode(strArray[4]);
            expireDate = XmlUtility.expireDate(strArray[4], strArray[19]);
            
			elementMap.put("record_type", "public");
			elementMap.put("primary_id", primaryId);			
			elementMap.put("first_name",  StringEscapeUtils.escapeXml(firstName));
			elementMap.put("middle_name",  StringEscapeUtils.escapeXml(middleName));
			elementMap.put("last_name",  lastName);
            elementMap.put("full_name",  StringEscapeUtils.escapeXml(fullName));
			elementMap.put("pref_first_name", "");
			elementMap.put("pref_middle_name", "");
			elementMap.put("pref_last_name", "");
			elementMap.put("pref_name_suffix", "");
            elementMap.put("user_group", userGroup);
            elementMap.put("preferred_language", "en");
            elementMap.put("expiry_date", expireDate);
            elementMap.put("purge_date", expireDate);
            elementMap.put("account_type", "EXTERNAL");
            elementMap.put("status", "ACTIVE");
          
		} catch (Exception e) {
			e.printStackTrace();
		}
   	    return elementMap;   	    
	}

	private static Element contactInfoElement(Document doc, String lineIn) {
		Element contactInfo = null;
		Map tmpMap = null;
		try {			
			 contactInfo  = doc.createElement("contact_info");
		     if(XmlUtility.includeBlock(lineIn, "mailcode", null, XmlUtility.EMPLOYEE)) {
			     Element addresses = doc.createElement("addresses");
			     Element address = XmlUtility.addressBlock(doc,"address",lineIn, true, "work");			 
	             addresses.appendChild(address);
	             contactInfo.appendChild(addresses);
		     }
	         if (XmlUtility.includeBlock(lineIn, "email", null, XmlUtility.EMPLOYEE)) {
	             Element emails = doc.createElement("emails");
	             Element email = XmlUtility.emailBlock(doc,"email",lineIn, true, "work");
	             emails.appendChild(email);
	             contactInfo.appendChild(emails);
	         }
	         if (XmlUtility.includeBlock(lineIn, "phone", null, XmlUtility.EMPLOYEE)) {
		         Element phones = doc.createElement("phones");
		         Element phone = XmlUtility.phoneBlock(doc,"phone",lineIn, true, "office");
		         phones.appendChild(phone);
		         contactInfo.appendChild(phones);
	         }
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return contactInfo;  	    
	}
	
	private static Element userIdElement(Document doc, String lineIn, String sysIdToken) {
		Element userIds = null;
		Map tmpMap = null;
		try {			
			 userIds  = doc.createElement("user_identifiers");
			 Element userIdElement = null;
	         if(XmlUtility.includeBlock(lineIn, "BARCODE", null, XmlUtility.EMPLOYEE)) {
	        	 userIdElement = XmlUtility.userIdBlock(doc,"user_identifier",lineIn,"BARCODE",XmlUtility.EMPLOYEE, sysIdToken);			 
	             userIds.appendChild(userIdElement);
	         }
	         if(XmlUtility.includeBlock(lineIn, "UNIV_ID", null, XmlUtility.EMPLOYEE)) {
	             userIdElement = XmlUtility.userIdBlock(doc,"user_identifier",lineIn,"UNIV_ID",XmlUtility.EMPLOYEE, sysIdToken);			 
	             userIds.appendChild(userIdElement);
	         }
	         userIdElement = XmlUtility.userIdBlock(doc,"user_identifier",lineIn,"OTHER_ID_2",XmlUtility.EMPLOYEE, sysIdToken);			 
	         userIds.appendChild(userIdElement);
	         userIdElement = XmlUtility.userIdBlock(doc,"user_identifier",lineIn,"OTHER_ID_4",XmlUtility.EMPLOYEE, sysIdToken);			 
	         userIds.appendChild(userIdElement);
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return userIds;  	    
	}
		
	private static Element statisticElement(Document doc, String lineIn, Map statsMap) {
		Element userIds = null;
		Map tmpMap = null;
		try {			
			 userIds  = doc.createElement("user_statistics");
			 Element userIdElement = XmlUtility.statsBlock(doc,"user_statistic",lineIn, statsMap, XmlUtility.EMPLOYEE);			 
	         userIds.appendChild(userIdElement);
	        
	    } catch (Exception e) {
	         e.printStackTrace();
	    }
		return userIds;  	    
	}
}
