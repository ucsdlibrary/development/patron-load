package edu.ucsd.library.patronload.apps;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.Reader;

import java.io.IOException;

import javax.naming.InitialContext;

import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ucsd.library.util.FileUtils;

public class getxmlfile extends HttpServlet {
	private String marcFilesDir;

	public void init(ServletConfig conf) throws ServletException {
        InitialContext jndi = null;
        try {
            jndi = new InitialContext();
            marcFilesDir = (String)jndi.lookup("java:comp/env/marcFilePath");
        } catch (NamingException e) {
            e.printStackTrace();
        }
		super.init(conf);
	}

	public void service(HttpServletRequest req, HttpServletResponse res)
		throws ServletException, IOException {
		ServletOutputStream out = null;
		HttpSession mySession = req.getSession(true);

		String contextDir =
			getServletConfig().getServletContext().getRealPath("")
				+ File.separator;

		String fileToGet = req.getParameter("file");

		if (fileToGet != null) {
				String pathToFile = marcFilesDir + fileToGet;

				if ((new File(pathToFile)).exists()) {

					res.setContentType("application/xml; charset=UTF-8");
					res.setHeader(
						"Content-disposition",
						"attachment; filename=" + fileToGet);
					out = res.getOutputStream();
					
					Reader inputReader = new InputStreamReader(new FileInputStream(pathToFile),"UTF-8");
					BufferedReader br = new BufferedReader(inputReader);
		        	String lineIn = null;

					while (((lineIn = br.readLine()) != null)
							&& !(lineIn.trim().equals(""))) {
						lineIn = lineIn.trim();	
					    out.write(lineIn.getBytes("UTF-8"));
					}
				} else {
					out.print("Error: filename not found!");
				}
				out.flush();
				out.close();
		} else {
			out.print("Error: parameter \"file\" not found in request!");
		}
	}
}
