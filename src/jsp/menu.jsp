<%@ page errorPage="error_pages/error.jsp" %>

<HTML>
<HEAD>
<TITLE>menu</TITLE>

<script src="../html/scripts/detect_browser.js"></script>

<SCRIPT LANGUAGE="JavaScript">
<!--

function logout() {

	if (is_nav4 || is_nav3) {
		alert("Use the logout button in the other window.");
	} else {

		if (parent.window.opener) {
			parent.window.opener.document.form1.submit(); parent.window.close();
		} else {
			alert("You have closed the parent window. Logging out cannot continue!");
		}
	}
}
// -->
</SCRIPT>

</HEAD>

<BODY background="../images/background.gif">

<br>

<font face="Verdana, Arial, sans-serif" size=2 class="fontNormal" color="#FFFFFF">
<center>
<b>Choose an action</b>
<hr>
</center>
</font>

<br>
<table>

<tr>
<td bgcolor='#6092C3' onClick="parent.frames['workarea'].location='change_settings.jsp'" onMouseOver="this.style.backgroundColor='#6161C2'; this.style.color='white'; this.style.cursor='hand';" onMouseOut="this.style.backgroundColor='#6092C3'; this.style.color='black'">
<font face="Verdana, Arial, sans-serif" size=2 class="fontNormal">
<img src="../images/downarrow.gif">
<b><a href="change_settings.jsp" target="workarea">Change Settings</a></b>
</font>
</td>
</tr>

<tr>
<td>
&nbsp;
</td>
</tr>

<tr>
<td bgcolor='#6092C3' ONCLICK="parent.frames['workarea'].location='changeProperties.jsp'" onMouseOver="this.style.backgroundColor='#6161C2'; this.style.color='white'; this.style.cursor='hand';" onMouseOut="this.style.backgroundColor='#6092C3'; this.style.color='black'">
<font face="Verdana, Arial, sans-serif" size=2 class="fontNormal">
<img src="../images/downarrow.gif">
<b><a href="changeProperties.jsp" target="workarea">Change Properties</a></b>
</font>
</td>
</tr>

<tr>
<td>
&nbsp;
</td>
</tr>

<tr>
<td bgcolor='#6092C3' ONCLICK="parent.frames['workarea'].location='download_employees_xml.jsp.jsp'" onMouseOver="this.style.backgroundColor='#6161C2'; this.style.color='white'; this.style.cursor='hand';" onMouseOut="this.style.backgroundColor='#6092C3'; this.style.color='black'">
<font face="Verdana, Arial, sans-serif" size=2 class="fontNormal">
<img src="../images/downarrow.gif">
<b><a href="download_employees_xml.jsp" target="workarea">Download XML Files</a></b>
</font>
</td>
</tr>

<tr>
<td>
&nbsp;
</td>
</tr>

<tr>
<td bgcolor='#6092C3' ONCLICK="parent.frames['workarea'].location='create_all_employee_xml.jsp'" onMouseOver="this.style.backgroundColor='#6161C2'; this.style.color='white'; this.style.cursor='hand';" onMouseOut="this.style.backgroundColor='#6092C3'; this.style.color='black'">
<font face="Verdana, Arial, sans-serif" size=2 class="fontNormal">
<img src="../images/downarrow.gif">
<b><a href="create_all_employee_xml.jsp" target="workarea">Create XML File</a></b>
</font>
</td>
</tr>

<tr>
<td bgcolor='#6092C3' ONCLICK="parent.frames['workarea'].location='create_inc_xml.jsp'" onMouseOver="this.style.backgroundColor='#6161C2'; this.style.color='white'; this.style.cursor='hand';" onMouseOut="this.style.backgroundColor='#6092C3'; this.style.color='black'">
<font face="Verdana, Arial, sans-serif" size=2 class="fontNormal">
<img src="../images/downarrow.gif">
<b><a href="create_inc_xml.jsp" target="workarea">Create Inc XML</a></b>
</font>
</td>
</tr>

<tr>
    <td bgcolor='#6092C3' ONCLICK="parent.frames['workarea'].location='select_xml.jsp'" onMouseOver="this.style.backgroundColor='#6161C2'; this.style.color='white'; this.style.cursor='hand';" onMouseOut="this.style.backgroundColor='#6092C3'; this.style.color='black'">
        <font face="Verdana, Arial, sans-serif" size=2 class="fontNormal">
            <img src="../images/downarrow.gif">
            <b><a href="select_xml.jsp" target="workarea">Individual XML</a></b>
        </font>
    </td>
</tr>

<tr>
<td>
&nbsp;
</td>
</tr>

<tr>
    <td bgcolor='#6092C3' ONCLICK="parent.frames['workarea'].location='submit_xml.jsp.jsp'" onMouseOver="this.style.backgroundColor='#6161C2'; this.style.color='white'; this.style.cursor='hand';" onMouseOut="this.style.backgroundColor='#6092C3'; this.style.color='black'">
        <font face="Verdana, Arial, sans-serif" size=2 class="fontNormal">
            <img src="../images/downarrow.gif">
            <b><a href="submit_xml.jsp" target="workarea">Submit XML Files</a></b>
        </font>
    </td>
</tr>

<tr>
<td>
&nbsp;
</td>
</tr>

<tr>
<td bgcolor='#6092C3' ONCLICK="logout(); return true;" onMouseOver="this.style.backgroundColor='#6161C2'; this.style.color='white'; this.style.cursor='hand';" onMouseOut="this.style.backgroundColor='#6092C3'; this.style.color='black'">
<font face="Verdana, Arial, sans-serif" size=2 class="fontNormal">
<img src="../images/downarrow.gif">
<b><a href="" ONCLICK="logout(); return false;">Log off</a></b>
</font>
</td>
</tr>

</table>


</BODY>
</HTML>
