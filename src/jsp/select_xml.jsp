<%@ page import="java.io.*, java.util.*, edu.ucsd.library.util.*, java.text.*" %>
<%@ page errorPage="error_pages/error.jsp" %>

<jsp:useBean id="patronLoad" class="edu.ucsd.library.patronload.beans.patronload_bean" scope="session"/>

<html>
	
	<body background="../images/background.gif">
		
		<br>
		
		<font face="Verdana, Arial, sans-serif" size=2 class="fontNormal" color="#FFFFFF">
			<b>Enter Comma-separated list of IDs for XML</b>
			<hr>
		</font>
		<br>
		
		<form action="select_xml2.jsp" method="post">
			
			<table align="center">
				
				<tr>
					<td>
						<font face="Verdana, Arial, sans-serif" size=2 class="fontNormal" color="#FFFFFF">
							<b>IDs:</b>
						</font>
					</td>
					<td>
					    <textarea id="idList" name="idList" rows="15"></textarea>
					</td>
				</tr>

			</table>
			
			<br><br>
			<center>
				<input type="submit" value="Submit">
			</center>
			
		</form>
		
	</body>
</html>
