# Patron Load

Patron Load is a Helm chart that leverages the [patronload][patronload] container
image to support easy deployment via Helm and Kubernetes.

## TL;DR;

```console
$ git clone https://gitlab.com/ucsdlibrary/development/patron-load.git
$ cd patron-load
$ helm install my-release chart/
```

## Introduction

This chart bootstraps a [patronload][patronload] deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ helm install my-release chart
```

The command deploys Patron Load on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Parameters

The following tables lists the configurable parameters of the Patron Load chart and their default values, in addition to chart-specific options.

| Parameter | Description | Default | Environment Variable |
| --------- | ----------- | ------- | -------------------- |
| `image.repository` | patronload image repository | `registry.gitlab.com/ucsdlibrary/development/patron-load` | N/A |
| `image.tag` | patronload image tag to use | `stable` | N/A |
| `image.pullPolicy` | patronload image pullPolicy | `Always` | N/A |
| `imagePullSecrets` | Array of pull secrets for the image | `[]` | N/A |
| `existingSecret.name` | Name of existing Secret in Deployment namespace to use instead of default Chart Secret.  | `patronload` | `N/A` |
| `existingSecret.enabled` | Whether to use an existing Secret for Deployment rather than default Chart secret.  | `patronload` | `N/A` |
| `ldap.username` | LDAP username for authentication.  | `nil` | `LDAP_USER` |
| `ldap.password` | LDAP password for authentication.  | `nil` | `LDAP_PASSWORD` |
| `minio.awsAccessKey` | Minio AccessKey for loading database backup.  | `nil` | `AWS_ACCESS_KEY_ID` |
| `minio.awsSecretKey` | Minio SecretKey for loading database backup.  | `nil` | `AWS_SECRET_ACCESS_KEY` |
| `minio.bucket` | Minio bucket that holds database backup.  | `nil` | `BUCKET` |
| `minio.endpoint` | Minio endpoint URL.  | `nil` | `ENDPOINT_URL` |
| `minio.cronBackupEnabled` | Enable a k8s CronJob to backup the `marcFilePath` to a minio bucket. For production only.  | `false` | N/A |
| `manager.enabled` | Toggle for enabling the Tomcat Manager applicastion.  | `false` | N/A |
| `persistence.enabled` | Toggle for enabling a PVC for the marcFilePath directory.  | `true` | N/A |
| `persistence.mountPath` | Path for marcFilePath.  | `/marcFilePath/` | `MARC_FILE_PATH` |
| `persistence.existingClaim.enabled` | Whether to use existing PVC for MARC_FILE_PATH.  | `false` | N/A |
| `persistence.existingClaim.name` | Name of existing PVC for MARC_FILE_PATH.  | `false` | N/A |
| `sftp.server` | Name of campus SFTP server.  | `nil` | `SFTP_SERVER` |
| `sftp.username` | SFTP server username.  | `nil` | `SFTP_USERNAME` |
| `sftp.password` | SFTP server password.  | `nil` | `SFTP_PASSWORD` |
| `sftp.path` | Path on SFTP server to start from.  | `nil` | `SFTP_PATH` |
| `sftp.known_hosts` | Name of `known_hosts` file to use for SFTP/SSH.  | `/patron-load/known_hosts` | `KNOWN_HOSTS` |


[patronload]:https://gitlab.com/ucsdlibrary/development/patron-load

