FROM openjdk:8 as builder

RUN apt-get update && apt install -y ant

WORKDIR /patron-load
COPY . /patron-load
RUN ant clean webapp

FROM tomcat:8.5.78-jdk8 as production
MAINTAINER "Matt Critchlow <mcritchlow@ucsd.edu">

RUN apt-get update && apt-get upgrade -y && apt install -y locales curl

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV TZ America/Los_Angeles

COPY --from=builder /patron-load/container/patronload.xml /usr/local/tomcat/conf/Catalina/localhost/

RUN mkdir /patron-load
COPY --from=builder /patron-load/container/entrypoint.sh /patron-load/
COPY --from=builder /patron-load/dist/patronload.war /usr/local/tomcat/webapps/

# Download adbridge-realm-0.1.jar to Tomcat /lib
RUN curl -o /usr/local/tomcat/lib/adbridge-realm.jar "https://gitlab.com/ucsdlibrary/development/adridge-realm/-/package_files/112504523/download"

ENTRYPOINT ["/patron-load/entrypoint.sh"]
CMD ["catalina.sh", "run"]

FROM production as development
COPY --from=builder /patron-load/container/development/tomcat-users.xml /usr/local/tomcat/conf
RUN cp -r /usr/local/tomcat/webapps.dist/manager /usr/local/tomcat/webapps/
COPY --from=builder /patron-load/container/development/manager-context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml
