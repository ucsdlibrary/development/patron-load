#!/usr/bin/env sh

echo "Ensuring /marcFilePath shell scripts are executable..."
chmod +x "${MARC_FILE_PATH}getAccessToken.sh"
chmod +x "${MARC_FILE_PATH}getSystemIdToken.sh"

echo "Adding known_hosts entry for $SFTP_SERVER"
ssh-keyscan "$SFTP_SERVER" >> "$KNOWN_HOSTS"

echo "Injecting context secrets..."
sed -i -e "s/__ALMA_AWS_ACCESS_KEY_ID__/$ALMA_AWS_ACCESS_KEY_ID/g" \
    -i -e "s/__ALMA_AWS_SECRET_ACCESS_KEY__/$ALMA_AWS_SECRET_ACCESS_KEY/g" \
    -i -e "s/__ALMA_BUCKET__/$ALMA_BUCKET/g" \
    -i -e "s|__ALMA_ENDPOINT_URL__|$ALMA_ENDPOINT_URL|g" \
    -i -e "s/__DB2USER__/$DB2USER/g" \
    -i -e "s/__DB2PASSWORD__/$DB2PASSWORD/g" \
    -i -e "s/__DB2HOST__/$DB2HOST/g" \
    -i -e "s/__DB2DATABASE__/$DB2DATABASE/g" \
    -i -e "s/__LDAP_USER__/$LDAP_USER/g" \
    -i -e "s/__LDAP_PASSWORD__/$LDAP_PASSWORD/g" \
    -i -e "s|__KNOWN_HOSTS__|$KNOWN_HOSTS|g" \
    -i -e "s|__MARC_FILE_PATH__|$MARC_FILE_PATH|g" \
    -i -e "s/__SFTP_SERVER__/$SFTP_SERVER/g" \
    -i -e "s/__SFTP_USERNAME__/$SFTP_USERNAME/g" \
    -i -e "s/__SFTP_PASSWORD__/$SFTP_PASSWORD/g" \
    -i -e "s|__SFTP_PATH__|$SFTP_PATH|g" "$CATALINA_HOME"/conf/Catalina/localhost/patronload.xml

exec "$@"
